#!/bin/bash

sdir=$(dirname "$0");
echo $(dirname "$0");
pushd .
cd $(dirname "$0")/..
pwd
rm ./web/css/stylesheets/* ./web/css/fonts/* ./web/js/compiled/*
bin/console assetic:dump
popd
