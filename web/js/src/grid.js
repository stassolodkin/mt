/**
 * Created by stassolodkin on 1/09/15.
 */

system_app.controller('GridController', ['$rootScope', 'webServices', '$scope', '$location', '$window', '$log', '$http', '$timeout', '$interval', '$sce', 'uiGridConstants',
							function ($rootScope, webServices, $scope, $location, $window, $log, $http, $timeout, $interval, $sce, uiGridConstants) {

	var paginationOptions = {
		pageNumber: 1,
		pageSize: 4
	};

	var sortOptions = {
		sortColumn: null,
		sortDirection: uiGridConstants.ASC
	};

	$scope.loading = false;

	$scope.gridOptions = {
		paginationPageSizes: [2, 4, 8],
		paginationPageSize: 4,
		useExternalPagination: true,
		useExternalSorting: true,
		onRegisterApi: function( gridApi ) {

			$scope.gridApi = gridApi;

			$scope.gridApi.core.on.sortChanged($scope, $scope.sortChanged);

			gridApi.pagination.on.paginationChanged($scope, $scope.paginationChanged);

			if(angular.isDefined(gridApi.selection)) {
				gridApi.selection.on.rowSelectionChangedBatch($scope, function (rows) {
					//alert("All selected");
					// This is the event where all rows are selected at once
				});
			}
		}
	};

	$scope.paginationChanged = function (newPage, pageSize) {

		paginationOptions.pageNumber = newPage;
		paginationOptions.pageSize = pageSize;

		$scope.loadGridOptions($scope.dataUrl);
	};


	$scope.sortChanged = function (grid, sortColumns) {

		if (sortColumns.length == 0) {
			sortOptions.sortColumn = null;
			sortOptions.sortDirection = uiGridConstants.ASC;
		} else {
			sortOptions.sortColumn = sortColumns[0].field;
			sortOptions.sortDirection = sortColumns[0].sort.direction;
		}

		$scope.loadGridOptions($scope.dataUrl);
	};

    $scope.gridLinkClick = function(url, loadIn) {

        if(loadIn === null || loadIn == "") {
            $window.location.href = url;
        } else {
            $rootScope.$broadcast('loadIn', [url, loadIn]);
        }
    };

	$scope.submitSelection = function (submitUrl, submitFields, loadIn) {

        //TODO: This array might not need to be traversed - just convert json array to javascript
        debugger;
		if($scope.gridApi.selection.getSelectedRows().length > 0) {

			var varsToSubmit = [];
			angular.forEach($scope.gridApi.selection.getSelectedRows(), function(row, rowKey) {

				// submitFields, even though an array, at the moment only supports one field
				angular.forEach(JSON.parse(submitFields), function(requiredField, requiredFieldKey) {
					varsToSubmit.push(row[requiredField])
				});
			});

			var submitArray = angular.toJson(varsToSubmit);
			var finalUrl = submitUrl + "/" + submitArray;
			if(loadIn === null || loadIn == "") {
				$window.location.href = finalUrl;
			} else {
				$rootScope.$broadcast('loadIn', [finalUrl, loadIn]);
			}
		} else {
			alert("Please make a selection");
		}

	};

	$scope.loadGridOptions = function (ajaxUrl) {

		//sortColumn = typeof sortColumn !== 'undefined' ? sortColumn : 'Id';
		$scope.dataUrl =  ajaxUrl;
		$scope.loading = true;
		var res = $http.post($scope.dataUrl, {
											json_only: '1',
											sort_column: sortOptions.sortColumn,
											sort_direction: sortOptions.sortDirection,
											page_number: paginationOptions.pageNumber,
											page_size: paginationOptions.pageSize
		});

		res.success(function(response, status, headers, config) {
			angular.extend($scope.gridOptions, response);
		});

		res.error(function(data, status, headers, config) {
			alert( "Unable to load data: " + JSON.stringify({data: data}));
		});

		res.finally(function () {
			$scope.loading = false;
		});
	};
}]);