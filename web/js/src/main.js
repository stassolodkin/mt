/**
 * Created by stassolodkin on 1/09/15.
 */

var system_app = angular.module('system_app', [
												'ngTouch',
												'ui.grid',
												'ui.grid.pagination',
												'ngSanitize',
												'ui.grid.selection',
												'ui.bootstrap',
												'angularFileUpload'
]);

system_app.config(function($interpolateProvider){
	$interpolateProvider.startSymbol('{[{').endSymbol('}]}');
});

//$http({method: $scope.method, url: $scope.url, cache: $templateCache}).
//    then(function(response) {
//        $scope.status = response.status;
//        $scope.data = response.data;
//    }, function(response) {
//        $scope.data = response.data || "Request failed";
//        $scope.status = response.status;
//    });

//system_app.config(['$httpProvider', function($httpProvider) {
//	$httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
//}]);

system_app.config(['$httpProvider', function($httpProvider) {
    $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
}]);

system_app.factory('webServices',['$http', function($http){

	return {
		getData : function(ajaxUrl){

			//return  $http.get(ajaxUrl).then(function(response) {
			//	return response.data;
			//});

            return $http({
                method: 'GET',
                url: ajaxUrl
            }).then(function successCallback(response) {

                return response.data;
                // this callback will be called asynchronously
                // when the response is available
            }, function errorCallback(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
            });
		}
	}
}]);

system_app.directive('ngSystemCompile', function($compile) {

	return {
		restrict: 'A',
		link: function(scope, element, attrs) {
			scope.$watch(attrs.ngSystemCompile, function(newValue, oldValue) {
				element.html(newValue);
				$compile(element.contents())(scope);
			});
		}
	}
});

//
//system_app.filter('to_trusted', ['$sce', function($sce){
//    return function(text) {
//        //return $sce.trustAsHtml(text);
//        return text;
//    };
//}]);

