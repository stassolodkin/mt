/**
 * Created by stassolodkin on 1/09/15.
 */
system_app.controller('PanelsController', ['webServices', '$scope', '$sce', function(webServices, $scope, $sce){

    $scope.loadPanelFromServer = function(ajaxUrl, panelId) {

        $scope[panelId] = null;
        $scope["is" + panelId + "Loading"] = true;

        webServices.getData(ajaxUrl).then(function (response) {
            $scope["is" + panelId + "Loading"] = false;
            $scope[panelId] = $sce.trustAsHtml(response); //Assign data received to $scope.data
        });
    };

    $scope.setContent = function(content, panelId) {
        $scope[panelId] = $sce.trustAsHtml(content);
    };
}]);

system_app.controller('SinglePanelController', ['$scope', '$attrs', function($scope, $attrs){

    $scope.$on('immediateParentLoad', function (event, data) {

        event.stopPropagation();

        var panelId = $attrs.id;
        $scope.setContent(data, panelId);
    });

    $scope.$on('loadIn', function (event, data) {

		var panelId = $attrs.id;
		if('id_' + data[1] == panelId) {
			//event.stopPropagation();
			$scope.loadPanelFromServer(data[0], panelId);
		}
	});

    $scope.$on('setContent', function(event, data) {

        var panelId = $attrs.id;
        if('id_' + data[1] == panelId) {
            $scope.setContent(data[0], panelId);
        }
    });
}]);

