/**
 * Created by stassolodkin on 1/09/15.
 */

// Global menu controller
system_app.controller('MenuController', ['$rootScope', '$scope', '$timeout',
							function ($rootScope, $scope, $timeout) {

    $scope.menuClick = function(uri, itemId)  {

        // Make the menu collapse if we are on an IPad or a smaller deice
        var width = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);

        if(width <= 1024) {
            var menuButton = document.getElementById('menu_button');
            $timeout(function() {
                menuButton.click();
            });
        }

        // Set current item and load
        $scope.currentItem = itemId;
        $rootScope.$broadcast('loadIn', [uri, "main_panel"]);
    };
}]);

// Submenu controller
system_app.controller('SubMenuController', ['$rootScope', '$scope',
    function ($rootScope, $scope) {

    collapseSubMenu();

    $scope.toggleSubMenu = function() {

        // If a submenu and the menu is not expanded from the side, expand from the side before showing the submenu
        var menuButton = document.getElementById('menu_button');
        if($rootScope.menuExpanded == false) {
            menuButton.click();
        }

        toggleArrow();
        $scope.showSubMenu = !$scope.showSubMenu;
    };

    $scope.$on('MenuCollapse', function (event, data) {
        $scope.$apply(collapseSubMenu());
    });

    function toggleArrow() {
        if($scope.arrow == "arrow-right") {
            $scope.arrow = "arrow-down";
        } else {
            $scope.arrow = "arrow-right";
        }
    }

    function collapseSubMenu() {
        $scope.arrow = "arrow-right";
        $scope.showSubMenu = false;
    }
}]);

system_app.directive('paddingDirective', function () {

    return {
        restrict:'A',
        compile: function (element, attr) {

            return function postLink(scope, element, attrs) {
                element.css({
                    'padding-left': (attrs.level-1) * 10 + 'px'
                });
            };
        }
    };
});

system_app.directive('slideable', function () {
    return {
        restrict:'C',
        compile: function (element, attr) {
            // wrap tag
            var contents = element.html();
            element.html('<div class="slideable_content" style="margin:0 !important; padding:0 !important" >' + contents + '</div>');

            return function postLink(scope, element, attrs) {

                // default properties
                attrs.duration = (!attrs.duration) ? '1s' : attrs.duration;
                attrs.easing = (!attrs.easing) ? 'ease-in-out' : attrs.easing;

                var width = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);

                if(width > 375) {
                    //element.css({
                    //    'overflow': 'hidden',
                    //    'width': '250px',
                    //    'transitionProperty': 'width',
                    //    'transitionDuration': attrs.duration,
                    //    'transitionTimingFunction': attrs.easing
                    //});
                } else {
                    //element.css({
                    //    'overflow': 'hidden',
                    //    'width': width + 'px',
                    //    'transitionProperty': 'height',
                    //    'transitionDuration': attrs.duration,
                    //    'transitionTimingFunction': attrs.easing
                    //});
                }
            };
        }
    };
});

system_app.directive('slideToggle', function($rootScope) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {

            var target = document.querySelector(attrs.slideToggle);
            if(!target) {
                return;
            }

            // Indicate what the initial state is
            var width = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
            $rootScope.menuExpanded = (width > 1024);


            element.bind('click', function() {

                if(!target) {
                    return;
                }

                var width = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);

                if(width > 1024) {
                    if (!$rootScope.menuExpanded) {
                        target.style.width = '250px';
                    } else {
                        $rootScope.$broadcast('MenuCollapse', null);
                        target.style.width = '35px';
                    }

                    target.style.height = '100vh';
                } else {
                    if (!$rootScope.menuExpanded) {
                        target.style.height = 'auto';
                    } else {
                        $rootScope.$broadcast('MenuCollapse', null);
                        target.style.height = '0px';
                    }

                    target.style.width = '100vh';
                }
                $rootScope.menuExpanded = !$rootScope.menuExpanded;
            });
        }
    }
});
