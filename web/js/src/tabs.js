/**
 * Created by stassolodkin on 1/09/15.
 */



system_app.controller('TabController', ['webServices', '$scope', '$sce', '$attrs', function(webServices, $scope, $sce, $attrs){

    this.tabId = null;

    $scope.$on('immediateParentLoad', function (event, data) {

        event.stopPropagation();

        var tabSetId = $attrs.tabSetId;
        $scope[tabSetId] = $sce.trustAsHtml(data);
    });

	$scope.$on('loadIn', function (event, data) {

		var tabsetId = $attrs.tabSetId;
		var myActiveTab = "activeTabIdFor" + tabsetId;
		var activeTabId = $scope[myActiveTab];
		if('id_tabs_' + data[1] == tabsetId) {
			$scope.loadTabFromServer(data[0], activeTabId, tabsetId);
		}
	});

    $scope.loadTabFromServer = function(ajaxUrl, tabId, tabSetId) {

		$scope.setActiveTab(tabId, tabSetId);
        $scope[tabSetId] = null;
		$scope["is" + tabSetId + "Loading"] = true;

        webServices.getData(ajaxUrl).then(function (response) {
			$scope["is" + tabSetId + "Loading"]  = false;
            $scope[tabSetId] = $sce.trustAsHtml(response); //Assign data received to $scope.data
        });
    };

    $scope.isActiveTab = function (tabId, tabsetId) {
		var myActiveTab = "activeTabIdFor" + tabsetId;
        return $scope[myActiveTab] === tabId;
    };

    $scope.setActiveTab = function(tabId, tabsetId) {
		var myActiveTab = "activeTabIdFor" + tabsetId;
		$scope[myActiveTab] = tabId;
    };
}]);
