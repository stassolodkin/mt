system_app.controller('FormController', ['$rootScope', 'webServices', '$scope', '$location', '$window', '$log', '$http', '$timeout', '$interval', '$sce',
	function ($rootScope, webServices, $scope, $location, $window, $log, $http, $timeout, $interval, $sce) {

		$scope.formData = {};

		$scope.setValue = function(el, vl) {

			$scope.formData[el] = vl;
		};

		$scope.setChoice = function(el, vl) {

			//debugger;
			//console.log('el: ' +  el);
			//console.log('vl: ' +  vl);
			//$scope.formData[el] = vl;

		};

		$scope.initCheckBoxes = function(fieldName, selected) {

			$scope.formData[fieldName] = [];

			var selectedArray = selected.split(",");
			for (i = 0; i < selectedArray.length; i++) {
				$scope.formData[fieldName]['id_' + selectedArray[i]] = true;
			}


		};

		$scope.initChoiceBoxes = function(multiple, fieldName, selected) {

			var fieldNameArray = fieldName.split(".");

			// A special case for the tax field
			if(fieldNameArray.length > 1 && fieldNameArray[1] == 'tax') {

				$scope.formData[fieldNameArray[0]][fieldNameArray[1]] = selected;

			} else {
				if (multiple) {
					$scope.formData[fieldName] = [];
					var selectedArray = selected.split(",");
					for (i = 0; i < selectedArray.length; i++) {
						$scope.formData[fieldName][i] = selectedArray[i];
					}
				} else {
					$scope.formData[fieldName] = selected;
				}
			}
		};

		$scope.processForm = function(processUrl, submitTargetContainer) {

            alert(processUrl);
            alert(processUrl);
			var formSubmitReturnData = $scope.formData;
			console.log(formSubmitReturnData);
			var newContent = "Trial Data";
			// Submit and get data

            $scope.loading = true;
            var res = $http.post(processUrl, formSubmitReturnData);
debugger;
            res.success(function(response, status, headers, config) {
                angular.extend($scope.gridOptions, response);
            });

            res.error(function(data, status, headers, config) {
                alert( "Unable to load data: " + JSON.stringify({data: data}));
            });

            res.finally(function () {
                $scope.loading = false;
            });


			if(!submitTargetContainer) {
				// If no set explicitly load in the immediate parent
				//$scope.$emit('immediateParentLoad', newContent);
			} else {
				// Otherwise load in the specified panel or tab
				//$rootScope.$broadcast('setContent', [newContent, submitTargetContainer]);
			}

		};

	}]);





