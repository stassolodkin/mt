
system_app.controller('DatepickerCtrl', function ($scope) {

    // Will set initial date which will be overwritten if a value is passed in to the widget
    $scope.today = function() {
        $scope.dt = new Date();
    };

    //$scope.today();


    $scope.clear = function () {
        $scope.dt = null;
    };

    // Disable weekend selection
    $scope.disabled = function(date, mode) {
        return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
    };

    //$scope.toggleMin = function() {
    //    $scope.minDate = $scope.minDate ? null : new Date();
    //};
    //
    //$scope.toggleMin();
    //$scope.maxDate = new Date(2020, 5, 22);

    $scope.open = function($event) {
        $scope.status.opened = true;
    };

    $scope.setDate = function(dateName, min, max, year, month, day) {

        $scope.setDateRange(min, max);
        $scope.formData[dateName] = new Date(year, month-1, day, 0, 0, 0, 0);
    };

    $scope.setDateRange = function(min, max) {

        miD = new Date(min);
        maD = new Date(max);

        $scope.minDate =miD;
        $scope.maxDate = maD;
    };

    $scope.dateOptions = {
        formatYear: 'yyyy',
        startingDay: 1,
        altInputFormats: ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate']
    };

    //$scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    //$scope.format = $scope.formats[0];

    $scope.status = {
        opened: false
    };

});

//system_app.directive('minDate', function() {
//    return {
//        restrict: 'A',
//
//        link: function(scope, elem, attrs) {
//            //alert(attrs.minimumDate);
//            var d = new Date();
//            //var d = new Date(attrs.minimumDate);
//            attrs.minDate = d;
//            //console.log(attrs);
//            console.log(d);
//        }
//    };
//});
