system_app.controller('FileUploadController', ['$scope', 'FileUploader', '$interpolate', '$compile', function($scope, FileUploader, $interpolate, $compile) {

    $scope.uploader = new FileUploader();

    $scope.uploader.onAfterAddingFile = function(item) {
        //console.log(item);

        //$scope.formData.widget = {filename: item.file.name};

        var currentItems = $scope.uploader.queue;
        var previousItem = currentItems[currentItems.length -2 ];

        // If previous item failed uploading, remove it from the queue and its errors
        if(typeof previousItem !== 'undefined') {
            if(typeof $scope.uploadErrors[previousItem.file.name] !== 'undefined' && $scope.uploadErrors[previousItem.file.name].length > 0) {
                $scope.uploadErrors[previousItem.file.name] = [];
                $scope.uploader.removeFromQueue(previousItem);
            }
        }

        // Update current items
        currentItems = $scope.uploader.queue;
        var fileAlreadyExists = false;
        for (var i=0; i < currentItems.length-1; i++){

            if(currentItems[i].file.name === item.file.name) {
                fileAlreadyExists = true;
                alert("A file with this name already exists in the queue.");
                $scope.uploader.removeFromQueue(item);
            }

        }

        if(!fileAlreadyExists) {
//debugger;
            $scope.uploader.addToQueue(item);
            $scope.uploadErrors[item.file.name] = [];

            //var ind = $scope.formatFileName(item.file.name);
            //$scope['progressType_' + $scope.elementName + '_' + ind] = "info";
            //$compile($scope);
            //$scope[item.file.name] = 'info';
            $scope.uploader.uploadItem(item);
        }
    };

    $scope.formatFileName = function(itemName) {
        var ind = itemName.replace(".", "_");
        ind = ind.replace(/\s/g, '');

        return ind;
    };

    $scope.formatSize = function(bytes, si) {
        var thresh = si ? 1000 : 1024;
        if(Math.abs(bytes) < thresh) {
            return bytes + ' B';
        }
        var units = si
            ? ['kB','MB','GB','TB','PB','EB','ZB','YB']
            : ['KiB','MiB','GiB','TiB','PiB','EiB','ZiB','YiB'];
        var u = -1;
        do {
            bytes /= thresh;
            ++u;
        } while(Math.abs(bytes) >= thresh && u < units.length - 1);
        return bytes.toFixed(1)+' '+units[u];
    };

    $scope.removeItem = function(item) {
        $scope.uploader.removeFromQueue(item);
        var index = $scope.formData[$scope.elementName].indexOf(item.file.name);
        $scope.formData[$scope.elementName].splice(index, 1);
    };

    $scope.uploader.onSuccessItem = function(item, response, status, headers) {

        angular.forEach(response, function(elem, key) {
//debugger;
            var ind = $scope.formatFileName(key);
            if (typeof elem !== 'undefined' && elem.length > 0) {

                //$scope['progressType_' + $scope.elementName + '_' + ind] = "danger";

                angular.forEach(elem, function(error) {
                    $scope.uploadErrors[item.file.name].push(error);
                });

            } else {
                //$scope['progressType_' + $scope.elementName + '_' + ind]  = "success";
                $scope.formData[$scope.elementName].push(key);
            }

            //$compile($scope);
        });
    };

    $scope.uploader.onErrorItem = function(item, response, status, headers) {
        console.log('Upload Error:');
        console.log(response);
    };

    //$scope.initItem = function(item) {
//debugger;
//        var ind = $scope.formatFileName(item.file.name);
        //$scope["progressType_" + $scope.elementName + "_" + ind] = "info";
        //$compile($scope.opa)(scope);
        //$compile($scope);
        //alert(ind);
    //};

    $scope.initElement = function(elementName) {
      $scope.elementName = elementName;
      $scope.formData[$scope.elementName] = [];
      $scope.uploadErrors = [];
      //$scope.progressType = [];
    };

    //$scope.uploadAllItems = function() {
    //    $scope.uploader.uploadAll();
    //};

}]);
