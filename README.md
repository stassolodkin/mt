MT
====

# Overview

This is a multi-tier account and UI generation framework based on Symfony and is in the middle of development.
The controller actions are only used to test the framework and are, therefore not the best to take a look at. The main code is mostly under the following directories:
 
web/js/src

src/Stas/SystemBundle/Resources/views

src/Stas/SystemBundle/UIComponents

