<?php

namespace Stas\SystemBundle\Controller\System;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Stas\SystemBundle\Utilities\SystemUrl as SystemUrl;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Stas\SystemBundle\Utilities\FileUtilities as FU;
use Symfony\Component\Routing\Router;

abstract class SystemController extends Controller {

	/** @var \Stas\SystemBundle\Entity\User $user */
	protected $user;

	public function renderMessage($content) {

//		$response = new Response(
//			$content,
//			Response::HTTP_OK,
//			array('content-type' => 'text/html')
//		);
//
//		return $response;

		$returnContent = array(
			'message'=>$content,
//			'test' => array(
//				'one' => 'фдсщ',
//				'two' => 'two',
//				'three' => 'three',
//				'four' => 'four',
//				'five' => 'five',
//				'six' => 'six',
//			)
		);

        $data = $this->render('StasSystemBundle:System:message.html.twig', $returnContent);

		return $data;
	}

	/**
	 * @return \Stas\SystemBundle\Entity\User
	 */
	public function getUser() {
		if ($this->user === null) {
			$this->user = $this->get('security.token_storage')->getToken()->getUser();
		}
		return $this->user;
	}

	/**
	 * @return \Stas\SystemBundle\Entity\Account
	 */
	public function getAccount() {

		$user = $this->getUser();
		$contact = $user->getContact();
		$account = $contact->getAccount();

		return $account;
	}




	public function getRequest() {
		return $this->container->get('request_stack')->getCurrentRequest();
	}

	/**
	 * This function returns a url with parameters separated by / after the hostname
	 *
	 * @param $route
	 * @param null $parameters
	 * @return string
	 */
	public function getUrl($route, $parameters=[]) {

	    /** @var \Symfony\Bundle\FrameworkBundle\Routing\Router $router */
		$router = $this->container->get('router');
//        $url = $router->generate($route);
        $url = $router->generate($route, $parameters);

//		if(is_array($parameters)) {
//			foreach($parameters as $parameter) {
//				$url .= '/' . $parameter;
//			}
//		} else if(!is_null($parameters)) {
//			$url .= '/' . $parameters;
//		}

		return $url;
	}

	/**
	 * This function creates a URL which can be loaded into any component on the
	 * page, specified by component name. Potential components are Tabs, Panel, Modal
	 *
	 * @param $route
	 * @param $parameters
	 * @param $target
	 * @return SystemUrl
	 */
	public function getTargetedUrl($route, $parameters, $target) {

		$router = $this->container->get('router');
		return new SystemUrl($router->generate($route), $parameters, $target);
	}

	protected function getObject() {}

	/**
	 * @Route("/fileupload", name="file_upload")
	 *
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function fileUploadAction(Request $request) {

		$uploadedFiles = [];
		$errors = [];
		$valid = true;
		$tmpUploadDirectory = $this->container->getParameter('storage.tmp_upload_directory');

		/** @var \Symfony\Component\HttpFoundation\File\UploadedFile $uploadedFile */
		foreach($request->files as $uploadedFile) {

			$originalName = $uploadedFile->getClientOriginalName();
			$originalExtension = $uploadedFile->getClientOriginalExtension();
			$realPath = $uploadedFile->getRealPath();
//            $pathInfo = $uploadedFile->getPathInfo();
			$size = $uploadedFile->getSize();
//            $clientSize = $uploadedFile->getClientSize();
			$maxFileSize = $uploadedFile->getMaxFilesize();
//            $mimeType = $uploadedFile->getMimeType();
//            $clientMimeType = $uploadedFile->getClientMimeType();
//            $mTime = $uploadedFile->getMTime();
//            $cTime = $uploadedFile->getCTime();
//            $aTime = $uploadedFile->getATime();
//            $extension = $uploadedFile->getExtension();
//            $fileInfo = $uploadedFile->getFileInfo();
			$error = $uploadedFile->getError();
//			$errorMessage = $uploadedFile->getErrorMessage();

            if($uploadedFile->isValid()) {


                // Local setup
                $accountId = $this->getAccount()->getId();
                $targetDirectory = $tmpUploadDirectory . DIRECTORY_SEPARATOR . $accountId . DIRECTORY_SEPARATOR . 'tmp';
                $externalFileName = $accountId . DIRECTORY_SEPARATOR . 'tmp' . DIRECTORY_SEPARATOR . $originalName;
                $targetAbsoluteFileName = $tmpUploadDirectory . DIRECTORY_SEPARATOR . $externalFileName;

                $uploadedFiles[$originalName] = [];

                if ($size > $maxFileSize) {
                    $uploadedFiles[] = [$externalFileName, "File size exceeds the maximum allowed ($maxFileSize)"];
                }

                if (!FU::englishFileName($originalName)) {
                    $uploadedFiles[$originalName][] = "File name contains invalid characters";
                }

                if (!FU::fileNameLengthIsLegitimate($targetAbsoluteFileName)) {
                    $uploadedFiles[$originalName][] = "Resulting file name is longer than 255 characters.";
                }

                if(!file_exists($targetDirectory)) {
                    // Create target directory
                    if (!mkdir($targetDirectory, 0777, true)) {
                        $uploadedFiles[$originalName][] = "Could not create destination directory";
                    }
                }

                if (!move_uploaded_file($realPath, $targetAbsoluteFileName)) {
                    $uploadedFiles[$originalName][] = "Could not move uploaded file";
                }
            } else {
                $uploadedFiles[$originalName] = [$uploadedFile->getErrorMessage()];
            }
		}

		// Build the response and return
		$response = new Response(json_encode($uploadedFiles));
//        $response->setContent(json_encode($uploadedFiles));
//		$response->setStatusCode(Response::HTTP_OK);
		$response->headers->set('Content-Type', 'application/json');

//        $response = new Response(json_encode($gridOptions));
//        $response->headers->set('Content-Type', 'application/json');

		return $response;
	}
}
