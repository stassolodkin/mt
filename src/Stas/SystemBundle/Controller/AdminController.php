<?php

namespace Stas\SystemBundle\Controller;

use Stas\SystemBundle\Controller\System\SystemController;
use Stas\SystemBundle\Entity\BO\MoneyWithTax;
use Stas\SystemBundle\Service\UIService as UI;
use Stas\SystemBundle\UIComponents as UIC;
use Stas\SystemBundle\Entity\BO\Product as Product;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class AdminController extends SystemController
{

    /**
     * @Route("/welcome", name="stas_system_welcome")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function welcomeAction()
    {
        return $this->renderMessage('Congratulations, you have made it into the system!');
    }

    /**
     * @Route("/contact", name="stas_system_contact")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function contactAction()
    {
        return $this->renderMessage('Contact Page');
    }

    /**
     * @Route("/contact_child_one", name="stas_system_contact_child_1")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function contactChildAction()
    {
        return $this->renderMessage('Contact Child Page');
    }

    /**
     * @Route("/contact_child_two", name="stas_system_contact_child_2")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function contactChildTwoAction()
    {
        return $this->renderMessage('Contact Child 2 Page');
    }

    /**
     * @Route("/homepage_1", name="stas_system_homepage_1")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function homepageOneAction()
    {
        return $this->renderMessage('Homepage Page 1');
    }

    /**
     * @Route("/services", name="stas_system_service")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function serviceAction()
    {
        return $this->renderMessage('Service Page');
    }

    /**
     * @Route("/services_child", name="stas_system_service_child")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function serviceChildOneAction()
    {
        return $this->renderMessage('Service Child Page');
    }

    /**
     * @Route("/services_childs_child", name="stas_system_servive_childs_child")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function serviceChildTwoAction()
    {
        return $this->renderMessage('Service Child\'s Child Page');
    }

    /**
     * @Route("/admin", name="stas_system_admin")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function adminAction()
    {
        return $this->renderMessage('My Admin');
    }

    /**
     * @Route("/ajax_one", name="stas_system_ajax_one")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function ajaxOneAction()
    {
        //return $this->renderMessage('AJAX One Return Value');

        $sql = 'SELECT t.*
				FROM TestData t
				WHERE t.id <= 3';

        $this->getDoctrine();
        /** @var UI $uiService */
        $uiService = $this->container->get('system.ui');
        $grid = $uiService->getGrid('MyGrid2', 'My Grid2', $sql);
        $grid->setTitle('My Grid 2');
        $grid->add('ID', 'id', $this->getUrl('stas_system_ajax_one'));
        $grid->add('Field 1', 'Field1');
        $grid->add('Field 2', 'Field2', $this->getUrl('stas_system_ajax_three'));
        //$grid->add('Pole 3', 'Field3', $this->getUrl('stas_system_ajax_three'));
        $grid->add('Field 4', 'Field4');
        $grid->add('Field 5', 'Field5', $this->getUrl('stas_system_ajax_three'));
//		$grid->add('Field 6', 'Field6', $this->getUrl('stas_system_ajax_three'), ['Field2', 'Field4']);
        $grid->add('Field 6', 'Field6', $this->getTargetedUrl(
            'stas_system_ajax_three',
            ['Field2', 'Field4'],
            'main_panel'
        ));
        $grid->add('Field 7', 'Field7', $this->getUrl('stas_system_ajax_three'));
        $grid->addAttribute(['enableFiltering'=>true]);

//        $grid->addAttribute(['rowTemplate' =>
//        '<div style="background-color: aquamarine" ng-click="grid.appScope.fnOne(row)"
//         ng-repeat="col in colContainer.renderedColumns track by col.colDef.name"
//         class="ui-grid-cell" ui-grid-cell></div>']);


        return $grid->render($this->getUrl('stas_system_ajax_one'));
    }

    /**
     * @Route("/ajax_two/{parameters}", name="stas_system_ajax_two")
     *
     * @param $parameters
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function ajaxTwoAction($parameters = null)
    {
        $parameters = json_decode($parameters);
        return $this->renderMessage('AJAX Two Return Value: ' . implode(",", $parameters));
        //return new RedirectResponse($this->generateUrl('system_training_grid'));
        //return $this->redirectToRoute('system_training_grid');
//		return $this->redirectToRoute('system_training_grid', array(), 301);
    }

    /**
     * @Route("/ajax_three", name="stas_system_ajax_three")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function ajaxThreeAction()
    {
        return $this->renderMessage('AJAX Three Return Value');
    }

    /**
     * @Route("/panels", name="system_training_panels")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function panelsAction()
    {

        /** @var UI $uiService */
        $uiService = $this->container->get('system.ui');
        $panels = $uiService->getPanels('MyPanels', 'My Panels');
        $panels->setTitle('My Panels');


        $panels->add('panel1', 'Panel 1', $this->getUrl('stas_system_ajax_one'));
        $panels->add('panel2', 'Panel 2', $this->getUrl('system_training_tabs'));
//		$panels->add('panel2', 'Panel 2', $router->generate('stas_system_ajax_three'));

        return $panels->render();
    }


    /**
     * @Route("/tabs", name="system_training_tabs")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function tabsAction()
    {

        /** @var UI $uiService */
        $uiService = $this->container->get('system.ui');
        $tabs = $uiService->getTabs('MyTabs', 'My Tabs');
        $tabs->setTitle('My Tabs');
        $tabs->add('tab1', 'Tab 1', $this->getUrl('stas_system_ajax_one'));
        $tabs->add('tab2', 'Tab 2', $this->getUrl('system_training_grid'));
        $tabs->add('tab3', 'Tab 3', $this->getUrl('stas_system_ajax_three'));
        $tabs->setActive('tab3');

        return $tabs->render();
    }

    /**
     * @Route("/grid", name="system_training_grid")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function gridAction()
    {
        $sql = 'SELECT t.*
				FROM TestData t
				WHERE t.id > 3';

        $this->getDoctrine();
        /** @var UI $uiService */
        $uiService = $this->container->get('system.ui');
        $grid = $uiService->getGrid('MyGrid', 'My Grid', $sql);
        $grid->setTitle('My Grid');
        $grid->add('ID', 'id', $this->getUrl('stas_system_ajax_one'));
        $grid->add('Field 1', 'Field1');
        $grid->add('Field 2', 'Field2', $this->getUrl('stas_system_ajax_three'));
        $grid->add('Pole 3', 'Field3', $this->getUrl('stas_system_ajax_three'));
        $grid->add('Field 4', 'Field4');
        $grid->add('Field 5', 'Field5', $this->getUrl('stas_system_ajax_three'));
        $grid->add('Field 6', 'Field6', $this->getUrl('stas_system_ajax_three'));
//		$grid->add('Field 7', 'Field7', $this->getUrl('stas_system_ajax_three'), ['Field5', 'Field3']);
        $grid->add('Field 7', 'Field7', $this->getTargetedUrl(
            'stas_system_ajax_three',
            ['Field5', 'Field3'],
            'panel1'
        ));
        $grid->enableRowSelectionSubmission(
            $this->getTargetedUrl('stas_system_ajax_two', null, 'panel1'),
            ['id'],
            true
        );
//		$grid->enableRowSelectionSubmission($this->getTargetedUrl('stas_system_ajax_two',
// null, 'MyTabs'), ['id'], true);

        return $grid->render($this->getUrl('system_training_grid'));
    }


    /** This section is for example of building and using Forms */


    /**
     * @Route("/inventory", name="stas_system_inventory")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function inventoryAction()
    {

        /** @var UI $uiService */
        $uiService = $this->container->get('system.ui');
        $panels = $uiService->getPanels('Inventory', 'Inventory Tracking System');
        $panels->add('ProductEdit', 'Product Edit Form', $this->getUrl('product_edit'));
        $panels->add('ProductManagement', 'Product Management', $this->getUrl('product_tabs'));
//		$panels->add('panel2', 'Panel 2', $router->generate('stas_system_ajax_three'));

        return $panels->render();
    }

    /**
     * @Route("/product/edit/{productId}", defaults={"productId" = 0}, name="product_edit")
     *
     * @param int $productId
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function productEditAction($productId, Request $request)
    {
        if (empty($productId)) {
            return $this->renderMessage('Select a product below to modify its data.');
        }

        $em = $this->getDoctrine()->getManager();

        /** @var \Stas\SystemBundle\Entity\BO\Product $product */
        $product = $em->getRepository('StasSystemBundle:BO\\Product')->find($productId);
        if (empty($product)) {
            throw new \Exception('No product found for ID ' . $productId);
        }

        /** @var UI $uiService */
        $uiService = $this->container->get('system.ui');
        $form = $uiService->getForm('Inventory', 'Create Product');

        $form->setTitle('Create Product');
//        $form->setSubmitTargetContainer('main_panel');

        $myText = $form->add('Text', 'name_field', 'Name', null, ['placeholder' => 'opanki', 'hint' => 'Product name']);
        $myText->setRequired();
//        $myText->setDisabled();
        $myText->setFieldValue($product->getName());

        ////////////////// Start Date ////////////////////
//		$myDate = $form->add('DateTime', 'availableFrom_field', 'Available From');
        $myDate = $form->add('Date', 'availableFrom_field', 'Available From');
        $myDate->setRequired();

        $minDate = new \DateTime('now');
        $minDate->modify('- 3 day');
        $myDate->setMinimumDate($minDate);

        $maxDate = new \DateTime('now');
        $maxDate->modify('+ 2 days');
        $myDate->setMaximumDate($maxDate);

        $pDate = $product->getAvailableFrom();
        $myDate->setFieldValue($pDate);
        ////////////// Finish Date ///////////////

        $myTextArea = $form->add('Textarea', 'description_field', 'Description');
        $myTextArea->setFieldValue($product->getDescription());
        $myTextArea->setDisabled();

        // Money with Tax field
        $myMoney = $form->add('MoneyWithTax', 'price', 'Price');
        $myTax = $em->getRepository('StasSystemBundle:BO\\Tax')->find(2);

        // This $myMoneyData object will come from a business object
        $myMoneyData = new MoneyWithTax(76, 4, $myTax);
        $myMoney->setFieldValue($myMoneyData);
        // Money with Tax field

        $form->addColumn();

        $mySerial = $form->add('Text', 'serial_field', 'Serial');
        $mySerial->setFieldValue($product->getSerialNumber());

        $myProductType = $form->add('ChoiceEntity', 'type_field', 'Type');
        $myProductType->setChoiceClass('StasSystemBundle:BO\\ProductType');
        $myProductType->setChoiceField('name');
        $myProductType->setFieldValue($product->getProductType());








// Multiselect
        $myChoice = $form->add('Choice', 'choice_field', 'Choice Test');
        $choices = ['1 Label' => '1', '2 Label' => '2'];
        $myChoice->setChoices($choices);
        $myChoice->setFieldValue(['2']);

        $myInteger = $form->add('Integer', 'my_integer_field', 'Integer Test');
        $myInteger->setFieldValue(5);

        $myNumber = $form->add('Number', 'my_number_field', 'Number Test');
        $myNumber->setScale(2);
        $myNumber->setFieldValue(0.55);

        $form->addColumn();

        $myCheckBox = $form->add('Checkbox', 'serialized_field', 'Serialized');
        $myCheckBox->setFieldValue($product->getProductType()->getSerialized());
//		$myCheckBox->setFieldValue(false);

        $myCategory = $form->add('CheckboxEntity', 'category_field', 'Category');
        $myCategory->setChoiceClass('StasSystemBundle:BO\\ProductCategory');
        $myCategory->setChoiceField('name');
        $myCategory->setFieldValue([$product->getProductCategory()]);


        // Checkboxes
        $myChoice = $form->add('Checkboxes', 'checkboxes_field', 'Checkboxes Test');
        $choices = ['1 Label' => '1', '2 Label' => '2'];
        $myChoice->setChoices($choices);
        $myChoice->setFieldValue(['2']);

        // Radio
        $myChoice = $form->add('Radio', 'radio_field', 'Radio Test');
        $choices = ['1 Label' => '1', '2 Label' => '2'];
        $myChoice->setChoices($choices);
        $myChoice->setFieldValue('2');

        // Dropdown
        $myChoice = $form->add('Dropdown', 'dropdown_field', 'Dropdown Test');
        $choices = ['1 Label' => '1', '2 Label' => '2'];
        $myChoice->setChoices($choices);
        $myChoice->setFieldValue('2');

        $myFile = $form->add('File', 'attachment', 'Upload');
        $myFile->addAttribute(['extensions' => 'html']);

        $form->addColumn();

        $myBrand = $form->add('RadioEntity', 'brand_field', 'Brand');
        $myBrand->setChoiceClass('StasSystemBundle:BO\\ProductBrand');
        $myBrand->setChoiceField('name');
        $myBrand->setFieldValue($product->getProductBrand());

        $form->add('Submit', 'submit', 'Create Product');
        $form->add('Button', 'button', 'Cancel');

        $response = $form->render();

        $actualForm = $form->getForm();
//        $requestData = json_decode(file_get_contents("php://input"));
        try {
            $actualForm->handleRequest($request);
        } catch (\Exception $e) {
            $x = 1;
        }
        $s = $actualForm->isSubmitted();
        $v = $actualForm->isValid();


        if ($actualForm->isSubmitted() && $actualForm->isValid()) {
            // do something in a moment
            $x = 3;
        } else {
            $t = $form->getBuilder()->getForm()->getErrors();
        }

        return $response;
    }

    /**
     * @Route("/product_tabs", name="product_tabs")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function productTabsAction()
    {

        /** @var UI $uiService */
        $uiService = $this->container->get('system.ui');
        $tabs = $uiService->getTabs('MyTabs', 'Product Management Tabs');

        $tabs->add('productList', 'Product List', $this->getUrl('product_management_list'));
        $tabs->add('productPrices', 'Pricing', $this->getUrl('stas_system_contact_child_1'));
        $tabs->setActive('productList');

        return $tabs->render();
    }

//	/**
//	 * @Route("/product_tabs", name="product_management_list")
//	 *
//	 * @return \Symfony\Component\HttpFoundation\Response
//	 */
//	public function productManagementAction() {
//
//		/** @var UI $uiService */
//		$uiService = $this->container->get('system.ui');
//		$tabs = $uiService->getTabs('ProductTabs', 'Product Management Tabs');
//
//		$tabs->add('productList', 'Product List', $this->getUrl('product_management_list'));
//		$tabs->add('productPrices', 'Pricing', $this->getUrl('system_training_grid'));
//		$tabs->setActive('productList');
//
//		return $tabs->render();
//	}

    /**
     * @Route("/product_list", name="product_management_list")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function gridProductList()
    {
        $sql = 'SELECT
						p.id,
						p.name as name,
						p.description as description,
						pt.name as product_type,
						pc.name as product_category
				FROM
						product p
						join product_type pt on p.product_type_id = pt.id
						join product_category pc on p.product_category_id = pc.id
				';

        //$this->getDoctrine();
        /** @var UI $uiService */
        $uiService = $this->container->get('system.ui');
        $grid = $uiService->getGrid('ProductList', 'Product List', $sql);
        $grid->setTitle('My Grid');
//		$grid->add('ID', 'id', $this->getUrl('product_edit'), ['id']);
        $grid->add('ID', 'id', $this->getTargetedUrl('product_edit', ['id'], 'ProductEdit'));
//		$grid->add('ID', 'id', $this->getTargetedUrl('product_edit', ['id'], 'MyTabs'));
        $grid->add('Type', 'product_type');
        $grid->add('Category', 'product_category');
        $grid->add('Name', 'name');
        $grid->add('Description', 'description');
        //$grid->enableRowSelectionSubmission($this->getTargetedUrl('stas_system_ajax_two', null,
        // 'panel2'), ['id'], true);
        //$grid->enableRowSelectionSubmission($this->getTargetedUrl('stas_system_ajax_two', null,
        // 'MyTabs'), ['id'], true);

        return $grid->render($this->getUrl('product_management_list'));
    }
}
