<?php

namespace Stas\SystemBundle\Controller;

use Stas\SystemBundle\Controller\System\SystemController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends SystemController {

	/**
	 * @Route("/", name="stas_system_homepage")
	 *
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function indexAction() {

		$securityContext = $this->container->get('security.authorization_checker');

		$options = [];
		if ($securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED') || $securityContext->isGranted('IS_AUTHENTICATED_FULLY')) {

			$user = $this->getUser();
			$contact = $user->getContact();
			$account = $contact->getAccount();

			$options = array(
				'user' => $user,
				'contact' => $contact,
				'account' => $account,
				);
		}

		return $this->render('StasSystemBundle:Page:index.html.twig', $options);
	}
}
