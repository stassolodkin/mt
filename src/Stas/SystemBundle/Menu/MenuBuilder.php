<?php
/**
 * Created by PhpStorm.
 * User: stassolodkin
 * Date: 18/08/15
 * Time: 12:36 AM
 */

namespace Stas\SystemBundle\Menu;

use Knp\Menu\FactoryInterface;
use Stas\SystemBundle\Entity\Account;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;

use Stas\SystemBundle\Entity\Feature as Feature;
use Stas\SystemBundle\Entity\Menu as Menu;

class MenuBuilder {

	private $factory;
	protected $container;
	private $userManager;
	private $token;

	/**
	 * @param FactoryInterface $factory
	 * @param ContainerInterface $container
	 */
	public function __construct(FactoryInterface $factory, ContainerInterface $container) {
		$this->factory = $factory;
		$this->container = $container;
		$this->token = $container->get('security.token_storage')->getToken();
		$this->userManager = $container->get('fos_user.user_manager');
	}

	public function mainMenu(RequestStack $request, array $options) {

		//$user = $this->token->getUser();

		$session = $request->getCurrentRequest()->getSession();
		$userId = $session->get('userId');
		$contactId = $session->get('contactId');
		$accountId = $session->get('accountId');

		$em = $this->container->get('doctrine.orm.entity_manager');

		/** @var Account $account */
		$account = $em->getRepository('StasSystemBundle:Account')->find($accountId);

		if (empty($account)) {
			throw new \Exception(
				'No account found for id ' . $accountId
			);
		}

		// Get the assigned menus
		$features = $account->getAccountFeatures();
		$accountMenu = array();
		/** @var Feature $feature */
		foreach($features->toArray() as $feature) {
			$featureMenu =  $feature->getMenu();
			$accountMenu = array_merge($accountMenu, $featureMenu->toArray());
		}

		/** @var \KNP\Menu\MenuItem $knpMenu */
		$knpMenu = $this->factory->createItem('root', array(
			'childrenAttributes'    => array(
				'id'				=> 'nav'
			),
		));

		/** @var Menu $menuItem */
		foreach($accountMenu as $menuItem) {

			/** @var Menu $menuTreeItem */
			$menuTree = $this->getMenuTree($menuItem); // get array of nodes from menuItem and to the very top parent

			foreach($menuTree as $menuTreeItem) {

				$parentItem = $menuTreeItem->getParent();

				if(!isset($parentItem)) { // No Parent - root menu, so add the item

                    if( empty( $knpMenu->getChild( $menuTreeItem->getName() ) ) ) {

                        $item = $knpMenu->addChild($menuTreeItem->getName(), array('route' => $menuTreeItem->getRoute()));

                        $item->setAttribute('id', 'id_' . $item->getName());
                        if (trim(strlen($menuTreeItem->getIcon())) > 0) {
                            $item->setAttribute('icon', 'fa fa-' . $menuTreeItem->getIcon());
                        }
                    }
				} else {
					$parentMenuItemName = $menuTreeItem->getParent()->getName();

                    // Find the parent in the menuTree and add the node to its parent
                    // This assumes that parents always come before children in the $menuTree array above
					$parentMenuItem = $this->getTreeNode($knpMenu, $menuTree, $parentMenuItemName);

                    $item = $parentMenuItem->addChild($menuTreeItem->getName(), array('route' => $menuTreeItem->getRoute()));
                    $item->setAttribute('id', 'id_' . $item->getName());
                    if (trim(strlen($menuTreeItem->getIcon())) > 0) {
                        $item->setAttribute('icon', 'fa fa-' . $menuTreeItem->getIcon());
                    }
				}
			}
		}

		return $knpMenu;
	}

	/**
     *
     * This function returns an array of menu items starting with th child that was passed in and
     * up to the very top parent, i.e.: [child, parent, grandparent]
     *
	 * @param Menu $menuItem
	 *
	 * @return array
	 */
	private function getMenuTree($menuItem) {

		$menuTree = [$menuItem];
		$directParent = $menuItem->getParent();
		while(isset($directParent)) {
			$menuTree[] = $directParent;
			$directParent = $directParent->getParent();
		}

//		return $menuTree;
		return array_reverse($menuTree);
	}

	/**
	 * The menuTree shows the exact path to follow down the knpMenu
	 * to find the menuItemName
	 *
	 * @param \Knp\Menu\MenuItem $knpMenu
	 * @param array $menuTree
	 * @param string $menuItemName
	 *
	 * @return \Knp\Menu\ItemInterface|\Knp\Menu\MenuItem|null
	 */
	private function getTreeNode($knpMenu, $menuTree, $menuItemName) {

		// Is this the node?
		if ($knpMenu->getName() == $menuItemName) {
			return $knpMenu;
		}

		if(count($menuTree) > 0) {

			$name = $menuTree[0]->getName();
			$nextKnpNode = $knpMenu[$name];

			return $this->getTreeNode($nextKnpNode, array_slice($menuTree, 1), $menuItemName);
		}

		return null;
	}
}
