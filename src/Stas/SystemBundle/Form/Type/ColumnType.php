<?php

namespace Stas\SystemBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ColumnType extends AbstractType {

    public function configureOptions(OptionsResolver $resolver) {

        $resolver->setDefaults(array(
            'data' => 'dummy',
        ));
    }

    public function getParent() {
        return TextType::class;
    }
}