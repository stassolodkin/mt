<?php

namespace Stas\SystemBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Stas\SystemBundle\Entity\BO\MoneyWithTax;
use Symfony\Component\Form\DataMapperInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class MoneyWithTaxType extends AbstractType implements DataMapperInterface {

    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder
            ->add('amount_no_tax', "Symfony\\Component\\Form\\Extension\\Core\\Type\\MoneyType", array(
                'divisor' => 100,
            ))
            ->add('amount_inc_tax', "Symfony\\Component\\Form\\Extension\\Core\\Type\\MoneyType", array(
                'divisor' => 100,
            ))
            ->add('tax', "Symfony\\Bridge\\Doctrine\\Form\\Type\\EntityType", array(
                'class' => "Stas\\SystemBundle\\Entity\\BO\\Tax",
                'choice_label' => 'name'
            ))
            ->setDataMapper($this)
        ;
    }

    public function configureOptions(OptionsResolver $resolver) {

        $resolver->setDefaults(array(
            'data_class' => "Stas\\SystemBundle\\Entity\\BO\\MoneyWithTax",
            'empty_data' => null
        ));
    }

    public function getBlockPrefix() {
        return "moneywithtax";
    }

    /**
     * @param MoneyWithTax $data
     * @param \Symfony\Component\Form\FormInterface[] $forms
     */
    public function mapDataToForms($data, $forms) {

        $forms = iterator_to_array($forms);
        $forms['amount_no_tax']->setData($data ? $data->getExTaxAmount() : 0);
        $forms['amount_inc_tax']->setData($data ? $data->getIncTaxAmount() : 0);
        $forms['tax']->setData($data ? $data->getTaxPercentage() : 0);
    }

    public function mapFormsToData($forms, &$data) {
        $forms = iterator_to_array($forms);
        $data = new MoneyWithTax(
            $forms['amount_no_tax']->getData(),
            $forms['amount_inc_tax']->getData(),
            $forms['tax']->getData()
        );
    }
}