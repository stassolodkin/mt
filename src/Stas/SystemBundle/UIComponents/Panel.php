<?php

namespace Stas\SystemBundle\UIComponents;

use Stas\SystemBundle\UIComponents\SystemUIComponent as BasicUIComponent;

/**
 * Panel
 */
class Panel extends BasicUIComponent {

	private $url;

	public function __construct($name, $title, $url) {
		parent::__construct();
		$this->setName($name);
		$this->setTitle($title);
		$this->setId("id_" . $name);
		$this->url = $url;
		$this->addClass('panel');
	}

	public function setUrl($url) {
		$this->url = $url;
		return $this;
	}

	public function getUrl() {
		return $this->url;
	}
}
