<?php

namespace Stas\SystemBundle\UIComponents;

use Stas\SystemBundle\UIComponents\SystemUIComponent as BasicUIComponent;
use Stas\SystemBundle\Utilities\SystemUrl as SystemUrl;

/**
 * Tabs
 */
class GridColumn extends BasicUIComponent {

    /** @var  SystemUrl $url */
	private $url;
	private $field;

	public function __construct($name, $field, $url) {
		parent::__construct();
		$this->setName($name);
		$this->field = $field;
		$this->url = $url;
		$this->addClass('grid_column');
	}

	/**
	 * @param $url
	 * @return $this
	 */
	public function setUrl($url) {
		$this->url = $url;
		return $this;
	}

	/**
	 * @return SystemUrl
	 */
	public function getUrl() {
		return $this->url;
	}

	/**
	 * @return mixed
	 */
	public function getUiGridUrl() {

		$url = $this->url->getUrl();
		foreach($this->url->getParameters() as $parameter) {
			$url .= '/{{row.entity.' . $parameter . '}}';
		}
		return $url;
	}

	/**
	 * @param $field
	 * @return $this
	 */
	public function setField($field) {
		$this->field = $field;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getField() {
		return $this->field;
	}

	/**
	 * @param bool|true $editable
	 */
	public function editable($editable = true) {
		$this->addAttribute(['enableCellEdit'=> $editable]);
	}

	/**
	 * @param bool|true $sortable
	 */
	public function sortable($sortable = true) {
		$this->addAttribute(['enableSorting'=> $sortable]);
	}
}
