<?php

namespace Stas\SystemBundle\UIComponents;

use Stas\SystemBundle\UIComponents\SystemUIComponent as BasicUIComponent;
//use Symfony\Component\Form\Extension\Core\Type\TextType;
//use Symfony\Component\Form\Extension\Core\Type\DateType;
//use Symfony\Component\Form\Extension\Core\Type\SubmitType;
//use Symfony\Component\Form\Extension\Core\Type\TextareaType;
//use Symfony\Bridge\Doctrine\Form\Type\EntityType;
//use StasSystemBundle\Form\Type\ColumnType;
use Symfony\Component\Form\FormFactory as FormFactory;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\DataTransformer\DateTimeToStringTransformer;
use Symfony\Component\Form\Extension\Core\DataTransformer\DateTimeToArrayTransformer;

/**
 * Form
 */
class Form extends BasicUIComponent {

	/** @var \Symfony\Component\Form\FormBuilder $builder */
	private $builder;

	private $fields;

	private $columns;

	private $submitTargetContainer;

	/**
	 * @param FormFactory $formFactory
	 * @param $templating
	 * @param $name
	 * @param $title
	 * @param null $data
	 * @param array $options
	 */
	public function __construct($formFactory, $templating, $name, $title, $data=null, $options=[]) {

		parent::__construct($templating);
		$this->setName($name);
		$this->setTitle($title);
		$this->setId("id_" . $name);

		$type = 'Symfony\Component\Form\Extension\Core\Type\FormType';
		$this->builder = $formFactory->createBuilder($type, $data, $options);

		$this->fields = [];
		$this->columns = 1;

		$this->submitTargetContainer = null;

		return $this;

	}

	/**
	 *
	 * Type of field from the Symfony form field type reference, omitting the \'Type\' part if the word
	 * @param string $type
	 *
	 * Name of the field
	 * @param $name
	 *
	 * Label to display
	 * @param $label
	 *
	 * Symfony form field attributes
	 * @param array $attrs
	 *
	 * Symfony form field options
	 * @param null $options
	 *
	 * @return FormField $this
	 */
	public function add($type, $name, $label, $options=[], $attrs=[]) {

		// The code further down expects attrs to be an array, even if empty. therefore converting
		// a potential null that was passed in into an empty array
		if($options === null) {
			$options = [];
		}

		if($attrs === null) {
			$attrs = [];
		}

		// Set some default options
		$options['label'] = $label;

		if($type != 'Submit' && $type != 'Button') {
			$options['required'] = false;
		}

		if($type == 'Date') {

			// Set defaults for the datetime field if don't exist
			if(!array_key_exists('input', $options)) {
//				$options['input'] = 'datetime';
				$options['input'] = 'datetime';
//				$options['data_class'] = '\DateTime';
//				$options['empty_data'] = null;
//				$options['widget'] = 'single_text';
			}

			$attrs['uib-datepicker-popup'] = 'dd-MMMM-yyyy';

		} else if ($type == 'ChoiceEntity') { // Drop down list

			// Default place holder
			$options['placeholder'] = 'Select an option';

		} else if ($type == 'CheckboxEntity') { // Set of checkboxes

			$options['expanded' ] = true;
			$options['multiple' ] = true;
			$attrs['checkboxset'] = "1";

		} else if ($type == 'RadioEntity') { // Set of radio buttons

			$options['expanded' ] = true;
			$options['multiple' ] = false;
            $attrs['radioset'] = "1";

		} else if ($type == 'Checkbox') {

            // Single checkbox
			$options['empty_data'] = false;

			// Default class for the checkbox
			if(array_key_exists('class', $attrs)) {
				$attrs['class'] = array_merge(['custom-checkbox'], $attrs['class']);
			} else {
				$attrs['class'] = 'custom-checkbox';
			}

		} else if ($type == 'Choice') {

            // Multiselect box
			$options['expanded' ] = false; // main
			$options['multiple' ] = true; // main

		} else if ($type == 'Checkboxes') {

            // Checkboxes not working
            $options['expanded' ] = true;
            $options['multiple' ] = true;
            $attrs['checkboxset'] = "1";
            $type = "Choice";

        } else if ($type == 'Radio') {

            // radio not working
            $options['expanded' ] = true;
            $options['multiple' ] = false;
            $attrs['radioset'] = "1";
            $type = "Choice";

        } else if ($type == 'Dropdown') {

            // radio not working
            $options['expanded' ] = false;
            $options['multiple' ] = false;
            $options['placeholder'] = 'Select an option';
            $type = "Choice";

        } else if ($type == 'Integer') {

//            $attrs['ng-pattern'] = "\d*";
			// Currently works as is - no additional options required

		} else if ($type == 'Number') {

			$attrs['pattern'] = "\d*(.\d{1,7})?";

		} else if ($type == 'Money') {

//            $attrs['tax'] = null;
			$options['currency'] = 'AUD';
		}

        if(strpos($type, 'Entity') !== FALSE) {
            $fieldClassType = "Symfony\\Bridge\\Doctrine\\Form\\Type\\EntityType";
        } else if($type == "MoneyWithTax") {
            $fieldClassType = "Stas\\SystemBundle\\Form\\Type\\MoneyWithTaxType";
        } else {
            $fieldClassType = "Symfony\\Component\\Form\\Extension\\Core\\Type\\" . $type . 'Type';
        }

		$field = new FormField($name, $fieldClassType, $options, $attrs, $this->getName());

		$this->fields[] = $field;

		return $field;
	}

	public function addColumn() {
		$this->columns++;
		$field = new FormField("Column_" . $this->columns, "Stas\\SystemBundle\\Form\\Type\\ColumnType", array('label' => false), [], $this->getName());

		$this->fields[] = $field;

		return $field;
	}

    /**
     * @param null $url
     * @return \Symfony\Component\HttpFoundation\Response
     */
	public function render($url = null) {

		// Iterate and add all fields
		/** @var FormField $field */
		foreach($this->fields as $field) {

			$fieldOptions = $field->getFieldOptions();
			if(!empty($field->getFieldValue())) {
				$fieldOptions['data'] = $field->getFieldValue();
			}

//            $type = $field->getFieldType();
//            if(strpos($type, 'DateTimeType') !== false) {
//                    $this->builder->add($field->getName(), $field->getFieldType(), $fieldOptions)->addViewTransformer(new DateTimeToArrayTransformer());
//            } else {
				$this->builder->add($field->getName(), $field->getFieldType(), $fieldOptions);
//            }
		}

		$this->builder->setMethod('POST');
//		$this->builder->setAction($url);
		$data = [
			'form_title' => $this->getTitle(),
			'form' => $this->builder->getForm()->createView(),
			'action' => $url,
			'submitTargetContainer' => $this->submitTargetContainer,
			'numberOfColumns' => $this->columns
		];

		return $this->templating->renderResponse('StasSystemBundle:UIComponents:form.html.twig', $data, null);
	}

	public function setSubmitTargetContainer($submitTargetName) {
		$this->submitTargetContainer = $submitTargetName;
	}

	/**
	 * @return \Symfony\Component\Form\FormBuilderInterface
	 */
	public function getBuilder() {
		return $this->builder;
	}

	public function getForm() {
		return $this->builder->getForm();
	}

	/**
	 * @param Request $request
	 */
	public function handleRequest(Request $request) {
		$this->builder->getForm()->handleRequest($request);
	}

	/**
	 * @return bool
	 */
	public function isSubmitted() {
		return $this->builder->getForm()->isSubmitted();
	}

	public function isValid() {
		return $this->builder->getForm()->isValid();
	}
}
