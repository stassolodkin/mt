<?php

namespace Stas\SystemBundle\UIComponents;

/**
 * Generic System UI Component
 */
abstract class SystemUIComponent {

	private $title;
	private $name;
	private $id;
	private $classes;
	private $attributes;

	/** @var \Symfony\Bundle\TwigBundle\TwigEngine $templating */
	protected $templating;

	public function __construct($templating=null) {
		$this->title = null;
		$this->id = null;
		$this->classes = [];
		$this->attributes = [];

		$this->templating = $templating;
	}

	public function setId($id) {
		$this->id = $id;
		return $this;
	}

	public function getId() {
		return $this->id;
	}

	public function setTitle($title) {
		$this->title = $title;
		return $this;
	}

	public function getTitle() {
		return $this->title;
	}

	public function setName($name) {
		$this->name = $name;
		return $this;
	}

	public function getName() {
		return $this->name;
	}

	public function addClass($class) {
		$this->classes[] = $class;
		return $this;
	}

	public function getClasses() {
		return $this->classes;
	}

	public function addAttribute($attribute) {
		if(is_array($attribute)) {
			$this->attributes = array_merge($this->attributes, $attribute);
		} else {
			$this->attributes[] = $attribute;
		}

		return $this;
	}

	public function getAttributes() {
		return $this->attributes;
	}
}
