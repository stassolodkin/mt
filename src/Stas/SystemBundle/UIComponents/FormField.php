<?php

namespace Stas\SystemBundle\UIComponents;

use Stas\SystemBundle\UIComponents\SystemUIComponent as BasicUIComponent;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * FormField class - stores data for a single form field
 */
class FormField extends BasicUIComponent {

	private $fieldType;
	private $fieldValue;

	// Symfony form field options values
	private $fieldOptions;

	public function __construct($name, $fieldType, $options=[], $attributes=[], $formName='form') {

		parent::__construct();
		$this->fieldType = $fieldType;
		$this->setName($name);

		// Combine the two arrays if required - as per the Symfony docs
		if(array_key_exists('attr', $options)) {
			$options['attr'] = array_merge($attributes, $options['attr']);
		} else {
			$options['attr'] = $attributes;
		}

		$this->fieldOptions = $options;
	}

	public function setFieldType($fieldType) {
		$this->fieldType = $fieldType;
		return $this;
	}

	public function getFieldType() {
		return $this->fieldType;
	}

	public function setFieldValue($fieldValue) {

//		if(is_a($fieldValue, 'DateTime')) {
//			$this->fieldValue = $fieldValue->format('Y-m-d H:i');
//		} else {
//			$this->fieldValue = $fieldValue;
//		}

		$this->fieldValue = $fieldValue;

		return $this;
	}

	/**
	 * @param \DateTime $date
	 */
	public function setMinimumDate($date) {

//        if(strpos($this->fieldType, 'DateTimeType') === FALSE) {
		if(strpos($this->fieldType, 'DateType') === FALSE) {
			throw new \Exception('Cannot set a minimum date on a non DateTime fields');
		}


		$this->fieldOptions['attr']['data-minimum-date'] = strtotime($date->format('d-m-Y')) * 1000;
		return $this;
	}

	/**
	 * @param \DateTime $date
	 */
	public function setMaximumDate($date) {

//        if(strpos($this->fieldType, 'DateTimeType') === FALSE) {
		if(strpos($this->fieldType, 'DateType') === FALSE) {
			throw new \Exception('Cannot set a maximum date on a non DateTime fields');
		}

		$this->fieldOptions['attr']['data-maximum-date'] = strtotime($date->format('d-m-Y')) * 1000;
		return $this;
	}

    /**
     * @param $attributes
     * @return $this
     */
	public function addAttribute($attributes) {

		if(is_array($attributes)) {
			foreach ($attributes as $key => $value) {
				$this->fieldOptions['attr'][$key] = $value;
			}
		} else {
			$this->fieldOptions['attr'][] = $attributes;
		}

		return $this;
	}

	public function getFieldValue() {
		return $this->fieldValue;
	}

	public function getFieldOptions() {
		return $this->fieldOptions;
	}

	public function setFieldOptions($options) {
		$this->fieldOptions = $options;
	}

	public function getFieldAttributes() {
		return $this->fieldOptions['attr'];
	}

	public function setFieldAttributes($attributes) {
		$this->fieldOptions['attr'] = $attributes;
	}

	public function setChoiceClass($choiceClass) {

		if(strpos($this->fieldType, 'Entity') !== FALSE) {
			$this->fieldOptions['class'] = $choiceClass;
		} else {
			throw new \Exception('You cannot set a data class for a non Entity field type');
		}
	}

	public function setChoiceField($choiceField) {

		if(strpos($this->fieldType, 'Entity') !== FALSE) {
			$this->fieldOptions['choice_label'] = $choiceField;
		} else {
			throw new \Exception('You cannot set a choice label for a non Entity field type');
		}

	}

	public function setChoices($choices) {

		$this->fieldOptions['choices'] = $choices;
	}

	public function setDisabled($disabled = true) {
		$this->fieldOptions['disabled'] = $disabled;
	}

	public function setRequired($required = true) {
		$this->fieldOptions['required'] = $required;
	}

	public function setScale($scale) {

		if(!is_integer($scale)) {
//            throw
		}

		$this->fieldOptions['attr']['pattern'] = "\d*(.\d{1,$scale})?";

	}
}
