<?php

namespace Stas\SystemBundle\UIComponents;

use Stas\SystemBundle\UIComponents\SystemUIComponent as BasicUIComponent;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Response as Response;
use Stas\SystemBundle\Utilities\SystemUrl as SystemUrl;

/**
 * Tabs
 */
class Grid extends BasicUIComponent {

	private $sortColumn;
	private $sortDirection;
	private $pageSize;
	private $pageNumber;

	/** @var \Doctrine\DBAL\Connection $db */
	private $db;
	private $query;
	private $columns;
	private $data;

	/* This section defines data require for row selection and submission */
	private $rowSelectionSubmissionURL;
	private $rowSelectionSubmissionFields;

	/**
	 * @param $db
	 * @param $templating
	 * @param string $name
	 * @param string $title
	 * @param string $dataQuery
	 * @param int $pageSize
	 */
	public function __construct($db, $templating, $name, $title, $dataQuery, $pageSize=20) {

		parent::__construct($templating);

		$this->columns = [];
		$this->addClass('system_grid');
		$this->setId("id_" . $name);

		$this->db = $db;
		$this->sormColumn = null;
		$this->sortDirection = 'asc';
		$this->pageSize = $pageSize;
		$this->pageNumber = 1;

		$this->data = [];

		if(isset($dataQuery)) {
			$this->query = $dataQuery;
		}

		$this->rowSelectionSubmissionURL = null;
		$this->rowSelectionSubmissionFields = [];
	}


    /**
     * @throws \Exception
     */
    private function getTotalItems() {

        $q = "select count(*) as count from (" . $this->query . ") s";
        try {
            /** @var \Doctrine\DBAL\Driver\PDOStatement $result */
            $result = $this->db->query($q);
        } catch (\Exception $e) {
            throw $e;
        }

        $row = $result->fetch();

        return $row['count'];
    }

	/**
	 * @throws \Exception
	 */
	private function loadData() {

		$q = $this->buildQuery();
		try {
			/** @var \Doctrine\DBAL\Driver\PDOStatement $result */
			$result = $this->db->query($q);
		} catch (\Exception $e) {
			throw $e;
		}

		$this->data = [];
		while ($row = $result->fetch()) {
			$this->data[] = $row;
		}

        return count($this->data);
	}

	/**
	 * @return string
	 */
	private function buildQuery() {
		return $this->getQueryWithLimit($this->getQueryWithOrder($this->query));
	}

	/**
	 * @param $query
	 * @return string
	 */
	private function getQueryWithOrder($query) {
		if(!is_null($this->sortColumn)) {
			$query .= "\nORDER BY " . $this->sortColumn . " " . $this->sortDirection;
		} else {
            $query .= "\nORDER BY 1 " . $this->sortDirection;
        }
		return $query;
	}

	/**
	 * @param $query
	 * @return string
	 */
	private function getQueryWithLimit($query) {

		$firstRecord = ($this->pageNumber - 1) * $this->pageSize;
		return $query . "\nLIMIT " . $firstRecord . ", " . $this->pageSize;
	}

	/**
	 * @param string $field
	 * @param string $title
	 * @param mixed $url
	 * @param array $parameters
	 * @return Grid $this
	 */
	public function add($title, $field, $url=null) {

		$column = new GridColumn($title, $field, $url);
		$column->setId("id_grid_column_" . $this->getName() . '_' . $title);

		$this->columns[$field] = $column;
		return $column;
	}

	/**
	 * @param SystemUrl $rowSelectionSubmissionURL
	 * @param $rowSelectionSubmissionFields
	 * @param bool|false $multiSelect
	 * @param null $reloadIn
	 * @throws \Exception
	 */
	public function enableRowSelectionSubmission($rowSelectionSubmissionURL, $rowSelectionSubmissionFields, $multiSelect=false) {

		if(!empty($rowSelectionSubmissionURL)) {
			$this->rowSelectionSubmissionURL = $rowSelectionSubmissionURL;
		} else {
			throw new \Exception("No URL specified for row selection submission");
		}

		if(!empty($rowSelectionSubmissionFields)) {
			$this->rowSelectionSubmissionFields = $rowSelectionSubmissionFields;
		} else {
			throw new Exception("No URL specified for row selection submission");
		}

		$this->addAttribute(['enableRowSelection' => true]);
		$this->addAttribute(['enableFullRowSelection' => true]);
		$this->addAttribute(['enableSelectAll' => true]);
		//$this->addAttribute(['selectionRowHeaderWidth' => 35]);
		//$this->addAttribute(['rowHeight' => 25]);
		//$this->addAttribute(['showGridFooter' => true]);
		if(!$multiSelect) {
			$this->addAttribute(['multiSelect' => false]);
		}
	}

	/**
	 * This return a null if the column is not set - do not remember why this is here
	 *
	 * @param $fieldName
	 * @return GridColumn|null
	 */
	public function getColumnByFieldName($fieldName) {
		return isset($this->columns[$fieldName]) ?  $this->columns[$fieldName] : null;
	}

	/**
	 * @return mixed
	 */
	public function getColumns() {
		return $this->columns;
	}

	/**
	 * @return null
	 */
	public function getData() {
		return $this->data;
	}

	/**
	 * @param bool|true $sortable
	 */
	public function sortable($sortable = true) {
		$this->addAttribute(['enableSorting'=> $sortable]);
	}

	/**
	 * @param bool|true $sortable
	 */
	public function externallySortable($sortable = true) {
		$this->addAttribute(['useExternalSorting'=> $sortable]);
	}

	/**
	 * @param null $dataUrl
	 * @return Response
	 */
	public function render($dataUrl = null) {

		$requestData = json_decode(file_get_contents("php://input"));
		if(isset($requestData) && isset($requestData->json_only) && $requestData->json_only == '1') {

			$this->sortColumn = isset($requestData->sort_column) ? $requestData->sort_column : null;
			$this->sortDirection = isset($requestData->sort_direction) ? $requestData->sort_direction : 'asc';
			$this->pageNumber = isset($requestData->page_number) ? $requestData->page_number : 1;
			$this->pageSize = isset($requestData->page_size) ? $requestData->page_size : 20;

            $gridOptions = [];
            $columnDefinitions = [];

			$rowsFetched = $this->loadData();

            $gridOptions['totalItems'] = $this->getTotalItems();
            $gridOptions['paginationCurrentPage'] = $this->pageNumber;


			/** @var GridColumn $column */
			foreach($this->columns as $column) {
				$columnDefinition = [
					'name' => $column->getName(),
					'field' => $column->getField()
				];

				if(!empty($column->getUrl())) {
					$columnDefinition['cellTemplate'] = '<div><a href="#" ng-click="grid.appScope.gridLinkClick(\'' . $column->getUiGridUrl() . '\', \'' . $column->getUrl()->getLoader() . '\')">{{COL_FIELD}}</a></div>';
//					$columnDefinition['cellTemplate'] = '<button ng-click="gridLinkClick(\'' . $column->getUiGridUrl() . '\', \'' . $column->getUrl()->getLoader() . '\')">{{COL_FIELD}}</button>';
				}

				$columnDefinition = array_merge($columnDefinition, $column->getAttributes());

				$columnDefinitions[] = $columnDefinition;
			}

			$gridOptions['columnDefs'] = $columnDefinitions;

			$data = [];
			foreach($this->data as $row) {

				$recordColumns = [];
				foreach($row as $columnField=>$columnData) {
					$columnObject = $this->getColumnByFieldName($columnField);
					if(!is_null($columnObject)) {

						$recordColumns = array_merge($recordColumns, [$columnField=>$columnData]);
					}
				}

				$data[] = $recordColumns;
			}

			$gridOptions['data'] = $data;
			$gridOptions = array_merge($gridOptions, $this->getAttributes());

			$response = new Response(json_encode($gridOptions));
			$response->headers->set('Content-Type', 'application/json');

			return $response;
		}

		$data = [
			'grid' => $this,
			'dataUrl' => $dataUrl,
			'submitURL' => $this->rowSelectionSubmissionURL,
			'submitFields' => json_encode($this->rowSelectionSubmissionFields)
		];

		return $this->templating->renderResponse('StasSystemBundle:UIComponents:grid.html.twig', $data, null);
	}
}
