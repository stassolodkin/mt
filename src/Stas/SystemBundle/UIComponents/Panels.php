<?php

namespace Stas\SystemBundle\UIComponents;

use Stas\SystemBundle\UIComponents\SystemUIComponent as BasicUIComponent;

/**
 * Panels
 */
class Panels extends BasicUIComponent {

	/** @var  \Stas\SystemBundle\UIComponents\Panel[] */
	private $panels;

	/**
	 * @param null $templating
	 * @param $name
	 * @param $title
	 */
	public function __construct($templating, $name, $title) {
		parent::__construct($templating);
		$this->tabs = [];
		$this->addClass('panels');
		$this->setId("id_panels_" . $name);
		$this->setTitle($title);
	}

	/**
	 * @param $name
	 * @param $title
	 * @param $url
	 * @return $this
	 */
	public function add($name, $title, $url) {

		$panel = new Panel($name, $title, $url);
		$this->panels[$name] = $panel;
		return $this;
	}

	public function getPanels() {
		return $this->panels;
	}

	public function render() {

		$data = array(
			'panels'=>$this
		);

		return $this->templating->renderResponse('StasSystemBundle:UIComponents:panels.html.twig', $data);
	}

}
