<?php

namespace Stas\SystemBundle\UIComponents;

use Stas\SystemBundle\UIComponents\SystemUIComponent as BasicUIComponent;
use Stas\SystemBundle\UIComponents\Tab as Tab;

/**
 * Tabs
 */
class Tabs extends BasicUIComponent {

	private $tabs;
	private $activeTab;

	/**
	 * @param null $templating
	 * @param $name
	 * @param $title
	 */
	public function __construct($templating, $name, $title) {
		parent::__construct($templating);
		$this->tabs = [];
		$this->addClass('tabs');
		$this->setId("id_tabs_" . $name);
	}

	/**
	 * @param $name
	 * @param $title
	 * @param $url
	 * @return $this
	 */
	public function add($name, $title, $url) {

		$tab = new Tab($name, $title, $url);
		$this->tabs[$name] = $tab;
		return $this;
	}

	public function getTabs() {
		return $this->tabs;
	}

	public function setActive($name) {

		if(isset($this->tabs[$name])) {
			$this->activeTab = $name;
		} else {
			throw new \Exception("Cannot set an active tab: $name does not exist");
		}
	}

	public function getActiveTab() {
		if(isset($this->activeTab)) {
			return $this->tabs[$this->activeTab];
		} else {
			return array_values($this->tabs)[0];
		}
	}

	public function render() {

		$data = array(
			'tabs'=>$this
		);

		$response = null;
		return $this->templating->renderResponse('StasSystemBundle:UIComponents:tabs.html.twig', $data, $response);
	}

}
