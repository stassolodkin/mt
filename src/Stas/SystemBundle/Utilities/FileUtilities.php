<?php

namespace Stas\SystemBundle\Utilities;


/**
 * FileUtilities
 */
class FileUtilities {

    /**
     *
     * Checks to make sure a given string is a legal, English file name
     *
     * @param $filename
     * @return bool
     */
    public static function englishFileName ($filename) {

        return (bool) ((preg_match("`^[-0-9A-Z_\.]+$`i",$filename)) ? true : false);
    }

    /**
     * Makes sure that the length of the file name is not longer than 255 characters
     *
     * @param $filename
     * @return bool
     */
    public static function fileNameLengthIsLegitimate ($filename) {
        return (bool) ((mb_strlen($filename,"UTF-8") < 225) ? true : false);
    }

}
