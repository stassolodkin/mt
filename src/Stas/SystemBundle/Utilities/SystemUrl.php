<?php

namespace Stas\SystemBundle\Utilities;


/**
 * SystemUrl
 */
class SystemUrl {

	private $url;
	private $parameters;
	private $loader;

	public function __construct($url, $parameters=null, $loader=null) {

		$this->url = $url;
		$this->parameters = $parameters;
		$this->loader = $loader;
	}

	/**
	 * @param $url
	 * @return $this
	 */
	public function setUrl($url) {
		$this->url = $url;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getUrl() {
		return $this->url;
	}

	/**
	 * @param $parameters
	 * @return $this
	 */
	public function setParameters($parameters) {
		$this->parameters = $parameters;
		return $this;
	}

	/**
	 * @return array|string
	 */
	public function getParameters() {
		return $this->parameters;
	}

	/**
	 * @param $loader
	 * @return $this
	 */
	public function setLoader($loader) {
		$this->loader = $loader;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getLoader() {
		return $this->loader;
	}

	public function generate() {

		$url = $this->url;
		if(is_array($this->parameters)) {
			foreach($this->parameters as $parameter) {
				$url .= '/' . $parameter;
			}
		} else if(!is_null($this->parameters)) {
			$url .= '/' . $this->parameters;
		}

		return $url;
	}
}
