<?php

namespace Stas\SystemBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * User
 *
* @ORM\Entity
* @ORM\Table(name="fos_user")
*/
class User extends BaseUser {

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	protected $id;

	/**
	 * @ORM\Column(name="login_attempts", type="integer", nullable=true)
	 */
	protected $failedLoginAttempts;

	/**
	 * @ORM\OneToOne(targetEntity="Contact", inversedBy="user")
	 * @ORM\JoinColumn(name="contact_id", referencedColumnName="id")
	 **/
	private $contact;

	/**
	 * @ORM\ManyToMany(targetEntity="Group")
	 * @ORM\JoinTable(name="fos_user_user_group",
	 *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
	 *      inverseJoinColumns={@ORM\JoinColumn(name="group_id", referencedColumnName="id")}
	 * )
	 */
	protected $groups;



	public function __construct() {

		parent::__construct();
		// your own logic
	}

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set failedLoginAttempts
     *
     * @param integer $failedLoginAttempts
     * @return User
     */
    public function setFailedLoginAttempts($failedLoginAttempts)
    {
        $this->failedLoginAttempts = $failedLoginAttempts;

        return $this;
    }

    /**
     * Get failedLoginAttempts
     *
     * @return integer 
     */
    public function getFailedLoginAttempts()
    {
        return $this->failedLoginAttempts;
    }

    /**
     * Set contact
     *
     * @param Contact $contact
     * @return User
     */
    public function setContact(Contact $contact = null)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get contact
     *
     * @return Contact
     */
    public function getContact()
    {
        return $this->contact;
    }
}
