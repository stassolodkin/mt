<?php

namespace Stas\SystemBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection as ArrayCollection;

/**
 * IdentificationType
 *
 * @ORM\Table(name="identification_type")
 * @ORM\Entity
 */
class IdentificationType extends SystemModel {

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	protected $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="value", type="string", length=100, nullable=false)
	 */
	private $value;

	/** Reverse relationships */

	/**
	 * @var ArrayCollection $identifications
	 *
	 * @ORM\OneToMany(targetEntity="Identification", mappedBy="type")
	 */
	protected $identifications;

	/**
	 * Set value
	 *
	 * @param string $value
	 * @return IdentificationType
	 */
	public function setValue($value) {

		$this->value = $value;

		return $this;
	}

	/**
	 * Get value
	 *
	 * @return string
	 */
	public function getValue() {
		return $this->value;
	}

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->identifications = new ArrayCollection();
    }

    /**
     * Add identification
     *
     * @param Identification $identification
     * @return IdentificationType
     */
    public function addIdentification(Identification $identification)
    {
        $this->identifications->set($identification->getId(), $identification);

        return $this;
    }

    /**
     * Remove identification
     *
     * @param Identification $identification
     */
    public function removeIdentification(Identification $identification)
    {
        $this->identifications->remove($identification->getId());
    }

    /**
     * Get identifications
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getIdentifications()
    {
        return $this->identifications;
    }
}
