<?php

namespace Stas\SystemBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AddressLine
 *
 * @ORM\Table(name="address_line")
 * @ORM\Entity
 */
class AddressLine extends SystemModel {

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	protected $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="line", type="string", length=255, nullable=false)
	 */
	private $line;

	/** Relationships */

	/**
	 * @var Address
	 *
	 * @ORM\ManyToOne(targetEntity="Address", inversedBy="addressLines")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="address_id", referencedColumnName="id")
	 * })
	 */
	protected $address;

	/**
	 * @var AddressLineType
	 *
	 * @ORM\ManyToOne(targetEntity="AddressLineType", inversedBy="addressLines")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="line_type_id", referencedColumnName="id")
	 * })
	 */
	protected $lineType;

	/**
	 * Set line
	 *
	 * @param string $line
	 * @return AddressLine
	 */
	public function setLine($line) {

		$this->line = $line;

		return $this;
	}

	/**
	 * Get line
	 *
	 * @return string
	 */
	public function getLine() {
		return $this->line;
	}

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

    /**
     * Set address
     *
     * @param Address $address
     * @return AddressLine
     */
    public function setAddress(Address $address = null)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return Address 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set lineType
     *
     * @param AddressLineType $lineType
     * @return AddressLine
     */
    public function setLineType(AddressLineType $lineType = null)
    {
        $this->lineType = $lineType;

        return $this;
    }

    /**
     * Get lineType
     *
     * @return AddressLineType 
     */
    public function getLineType()
    {
        return $this->lineType;
    }
}
