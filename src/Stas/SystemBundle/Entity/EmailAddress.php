<?php

namespace Stas\SystemBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EmailAddress
 *
 * @ORM\Table(name="email_address")
 * @ORM\Entity
 */
class EmailAddress extends SystemModel {

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	protected $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="value", type="string", length=100, nullable=false)
	 */
	private $value;


	/** Relationship definitions */

	/**
	 * @var EmailAddressType
	 *
	 * @ORM\ManyToOne(targetEntity="EmailAddressType", inversedBy="emailAddresses")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="type_id", referencedColumnName="id")
	 * })
	 */
	private $type;

	/**
	 * @var Contact
	 *
	 * @ORM\ManyToOne(targetEntity="Contact", inversedBy="emailAddresses")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="contact_id", referencedColumnName="id")
	 * })
	 */
	private $contact;



	/** Functions */

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Set value
	 *
	 * @param string $value
	 * @return EmailAddress
	 */
	public function setValue($value) {

		$this->value = $value;

		return $this;
	}

	/**
	 * Get value
	 *
	 * @return string
	 */
	public function getValue() {
		return $this->value;
	}

	/**
	 * Get contact
	 *
	 * @return Contact|null
	 */
	public function getContact() {

		return $this->contact;
	}


	/**
	 * Set contact
	 *
	 * @param Contact|null $contact
	 * @return EmailAddress
	 */
	public function setContact(Contact $contact) {

		$this->contact = $contact;
		return $this;
	}

	/**
	 * Set type
	 *
	 * @param EmailAddressType $type
	 * @return EmailAddress
	 */
	public function setType($type) {

		$this->type = $type;

		return $this;
	}

	/**
	 * Get type
	 *
	 * @return EmailAddressType
	 */
	public function getType() {
		return $this->type;
	}
}
