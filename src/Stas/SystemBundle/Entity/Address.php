<?php

namespace Stas\SystemBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection as ArrayCollection;

/**
 * Address
 *
 * @ORM\Table(name="address")
 * @ORM\Entity
 */
class Address extends SystemModel {

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	protected $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="post_code", type="string", length=100, nullable=true)
	 */
	private $postCode;

	/** Relationship definitions */

	/**
	 * @var State $state
	 *
	 * @ORM\ManyToOne(targetEntity="State", inversedBy="addresses")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="state_id", referencedColumnName="id")
	 * })
	 */
	private $state;

	/**
	 * @var State $state
	 *
	 * @ORM\ManyToOne(targetEntity="Contact", inversedBy="addresses")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="contact_id", referencedColumnName="id")
	 * })
	 */
	private $contact;

	/**
	 * @var Country $country
	 *
	 * @ORM\ManyToOne(targetEntity="Country", inversedBy="addresses")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="country_id", referencedColumnName="id")
	 * })
	 */
	private $country;

	/**
	 * @var AddressType $type
	 *
	 * @ORM\ManyToOne(targetEntity="AddressType", inversedBy="addresses")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="type_id", referencedColumnName="id")
	 * })
	 */
	private $type;


	/** Reverse relationships */

	/**
	 * @var ArrayCollection $addressLines
	 *
	 * @ORM\OneToMany(targetEntity="AddressLine", mappedBy="address")
	 * @ORM\OrderBy({"id" = "ASC"})
	 */
	protected $addressLines;


	/** Functions */

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Set postCode
	 *
	 * @param string $code
	 * @return Address
	 */
	public function setPostCode($code) {

		$this->postCode = $code;

		return $this;
	}

	/**
	 * Get postCode
	 *
	 * @return string
	 */
	public function getPostCode() {
		return $this->postCode;
	}

	/**
	 * Set state
	 *
	 * @param State $state
	 * @return Address
	 */
	public function setState($state) {

		$this->state = $state;

		return $this;
	}

	/**
	 * Get state
	 *
	 * @return string
	 */
	public function getState() {
		return $this->state;
	}

	/**
	 * Set state
	 *
	 * @param Contact $contact
	 * @return Address
	 */
	public function setContact($contact) {

		$this->contact = $contact;

		return $this;
	}

	/**
	 * Get state
	 *
	 * @return string
	 */
	public function getContact() {
		return $this->contact;
	}

	/**
	 * Set country
	 *
	 * @param Country $country
	 * @return Address
	 */
	public function setCountry($country) {

		$this->country = $country;

		return $this;
	}

	/**
	 * Get country
	 *
	 * @return Country
	 */
	public function getCountry() {
		return $this->country;
	}

	/**
	 * Set type
	 *
	 * @param AddressType $type
	 * @return Address
	 */
	public function setType($type) {

		$this->type = $type;

		return $this;
	}

	/**
	 * Get type
	 *
	 * @return AddressType
	 */
	public function getType() {
		return $this->type;
	}

	/**
	 * Add address line
	 *
	 * @param AddressLine $addressLine
	 * @return Address
	 */
	public function addAddressLine(AddressLine $addressLine) {

		$this->addressLines->set($addressLine->getId(), $addressLine);

		return $this;
	}

	/**
	 * Remove address line
	 *
	 * @param AddressLine $addressLine
	 * @return boolean
	 */
	public function removeAddressLine(AddressLine $addressLine) {

		return $this->addressLines->remove($addressLine->getId());
	}

	/**
	 * Get addressLines
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getAddressLines() {

		return $this->addressLines;
	}


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->addressLines = new ArrayCollection();
    }

}
