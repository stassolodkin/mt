<?php

namespace Stas\SystemBundle\Entity\BO;

use Stas\SystemBundle\Entity\SystemModel;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection as ArrayCollection;

/**
 * ProductType
 *
 * @ORM\Table(name="product_type")
 * @ORM\Entity
 */
class ProductType extends SystemModel {

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	protected $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="string", length=100, nullable=false)
	 */
	private $name;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="description", type="string", length=100, nullable=true)
	 */
	private $description;

    /**
     * @var boolean
     *
     * @ORM\Column(name="serialized", type="boolean", nullable=true)
     */
    private $serialized;

	/** Reverse relationships */

	/**
	 * @var ArrayCollection $products
	 *
	 * @ORM\OneToMany(targetEntity="Product", mappedBy="productType")
	 */
	protected $products;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->products = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return ProductType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return ProductType
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Add products
     *
     * @param \Stas\SystemBundle\Entity\BO\Product $products
     * @return ProductType
     */
    public function addProduct(\Stas\SystemBundle\Entity\BO\Product $products)
    {
        $this->products[] = $products;

        return $this;
    }

    /**
     * Remove products
     *
     * @param \Stas\SystemBundle\Entity\BO\Product $products
     */
    public function removeProduct(\Stas\SystemBundle\Entity\BO\Product $products)
    {
        $this->products->removeElement($products);
    }

    /**
     * Get products
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * Set serialized
     *
     * @param boolean $serialized
     * @return ProductType
     */
    public function setSerialized($serialized)
    {
        $this->serialized = $serialized;

        return $this;
    }

    /**
     * Get serialized
     *
     * @return boolean 
     */
    public function getSerialized()
    {
        return $this->serialized;
    }
}
