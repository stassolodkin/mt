<?php

namespace Stas\SystemBundle\Entity\BO;

use Stas\SystemBundle\Entity\SystemModel;
use Doctrine\ORM\Mapping as ORM;
//use Doctrine\Common\Collections\ArrayCollection as ArrayCollection;

/**
 * Tax
 *
 * @ORM\Table(name="tax")
 * @ORM\Entity
 */
class Tax extends SystemModel {

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	protected $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="string", length=100, nullable=false)
	 */
	private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="percentage", type="integer", nullable=false)
     */
    private $percentage;


	/** Functions */

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Set name
	 *
	 * @param string $name
	 * @return Tax
	 */
	public function setName($name) {

		$this->name = $name;

		return $this;
	}

	/**
	 * Get name
	 *
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * Get percentage
	 *
	 * @return $this->percentage
	 */
	public function getPercentage() {

		return $this->percentage;
	}

	/**
	 * Set percentage
	 *
	 * @param integer $percentage
	 * @return Tax
	 */
	public function setPercentage($percentage) {

		$this->percentage = $percentage;
		return $this;
	}
}
