<?php

namespace Stas\SystemBundle\Entity\BO;

use Stas\SystemBundle\Entity\SystemModel;
use Stas\SystemBundle\Entity\Country as Country;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection as ArrayCollection;

/**
 * ProductBrand
 *
 * @ORM\Table(name="product_brand")
 * @ORM\Entity
 */
class ProductBrand extends SystemModel {

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	protected $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="string", length=100, nullable=false)
	 */
	private $name;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="description", type="string", length=512, nullable=true)
	 */
	private $description;

	/** Relationships */

	/**
	 * @var Stas\SystemBundle\Entity\Country $country
	 *
	 * @ORM\ManyToOne(targetEntity="Stas\SystemBundle\Entity\Country", inversedBy="productBrands")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="country_id", referencedColumnName="id", nullable=true)
	 * })
	 */
	private $country;

	/** Reverse relationships */

	/**
	 * @var ArrayCollection $products
	 *
	 * @ORM\OneToMany(targetEntity="Product", mappedBy="productBrand")
	 */
	protected $products;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->products = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return ProductBrand
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return ProductBrand
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set country
     *
     * @param \Stas\SystemBundle\Entity\Country $country
     * @return ProductBrand
     */
    public function setCountry(\Stas\SystemBundle\Entity\Country $country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return \Stas\SystemBundle\Entity\Country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Add products
     *
     * @param \Stas\SystemBundle\Entity\BO\Product $products
     * @return ProductBrand
     */
    public function addProduct(\Stas\SystemBundle\Entity\BO\Product $products)
    {
        $this->products[] = $products;

        return $this;
    }

    /**
     * Remove products
     *
     * @param \Stas\SystemBundle\Entity\BO\Product $products
     */
    public function removeProduct(\Stas\SystemBundle\Entity\BO\Product $products)
    {
        $this->products->removeElement($products);
    }

    /**
     * Get products
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProducts()
    {
        return $this->products;
    }
}
