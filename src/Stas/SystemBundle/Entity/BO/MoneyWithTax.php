<?php

namespace Stas\SystemBundle\Entity\BO;

use Stas\SystemBundle\Entity\BO\Tax as Tax;

/**
 * Money With Tax
 */
class MoneyWithTax {

	private $exTaxAmount;
	private $incTaxAmount;

	/** @var Tax $taxPercentage */
	private $taxPercentage;

	public function __construct($exTaxAmount, $incTaxAmount, $taxPercentage) {
		$this->exTaxAmount = $exTaxAmount;
		$this->incTaxAmount = $incTaxAmount;
		$this->taxPercentage = $taxPercentage;
	}

	/**
	 * @param $amount
	 * @return $this
	 */
	public function setExTaxAmount($amount) {
		$this->exTaxAmount = $amount;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getExTaxAmount() {
		return $this->exTaxAmount;
	}

	/**
	 * @param $amount
	 * @return $this
	 */
	public function setIncTaxAmount($amount) {
		$this->incTaxAmount = $amount;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getIncTaxAmount() {
		return $this->incTaxAmount;
	}

	/**
	 * @param $percentage
	 * @return $this
	 */
	public function setTaxPercentage($percentage) {
		$this->taxPercentage = $percentage;
		return $this;
	}

	/**
	 * @return \Stas\SystemBundle\Entity\BO\Tax
	 */
	public function getTaxPercentage() {
		return $this->taxPercentage;
	}
}
