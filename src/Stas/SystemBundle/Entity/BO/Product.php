<?php

namespace Stas\SystemBundle\Entity\BO;

use Stas\SystemBundle\Entity\SystemModel;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection as ArrayCollection;

/**
 * Product
 *
 * @ORM\Table(name="product")
 * @ORM\Entity
 */
class Product extends SystemModel {

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	protected $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="string", length=100, nullable=false)
	 */
	private $name;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="description", type="string", length=100, nullable=true)
	 */
	private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="serial_number", type="string", length=100, nullable=true)
     */
    private $serialNumber;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="available_from", type="datetime", nullable=true)
     */
    private $availableFrom;

	/** Relationships */

	/**
	 * @var ProductBrand $productBrand
	 *
	 * @ORM\ManyToOne(targetEntity="ProductBrand", inversedBy="products")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="product_brand_id", referencedColumnName="id", nullable=true)
	 * })
	 */
	private $productBrand;

	/**
	 * @var ProductType $productType
	 *
	 * @ORM\ManyToOne(targetEntity="ProductType", inversedBy="products")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="product_type_id", referencedColumnName="id", nullable=true)
	 * })
	 */
	private $productType;

	/**
	 * @var ProductCategory $productCategory
	 *
	 * @ORM\ManyToOne(targetEntity="ProductCategory", inversedBy="products")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="product_category_id", referencedColumnName="id", nullable=true)
	 * })
	 */
	private $productCategory;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Product
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Product
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set productBrand
     *
     * @param \Stas\SystemBundle\Entity\BO\ProductBrand $productBrand
     * @return Product
     */
    public function setProductBrand(\Stas\SystemBundle\Entity\BO\ProductBrand $productBrand = null)
    {
        $this->productBrand = $productBrand;

        return $this;
    }

    /**
     * Get productBrand
     *
     * @return \Stas\SystemBundle\Entity\BO\ProductBrand 
     */
    public function getProductBrand()
    {
        return $this->productBrand;
    }

    /**
     * Set productType
     *
     * @param \Stas\SystemBundle\Entity\BO\ProductType $productType
     * @return Product
     */
    public function setProductType(\Stas\SystemBundle\Entity\BO\ProductType $productType = null)
    {
        $this->productType = $productType;

        return $this;
    }

    /**
     * Get productType
     *
     * @return \Stas\SystemBundle\Entity\BO\ProductType 
     */
    public function getProductType()
    {
        return $this->productType;
    }

    /**
     * Set productCategory
     *
     * @param \Stas\SystemBundle\Entity\BO\ProductCategory $productCategory
     * @return Product
     */
    public function setProductCategory(\Stas\SystemBundle\Entity\BO\ProductCategory $productCategory = null)
    {
        $this->productCategory = $productCategory;

        return $this;
    }

    /**
     * Get productCategory
     *
     * @return \Stas\SystemBundle\Entity\BO\ProductCategory 
     */
    public function getProductCategory()
    {
        return $this->productCategory;
    }

    /**
     * Set serialNumber
     *
     * @param string $serialNumber
     * @return Product
     */
    public function setSerialNumber($serialNumber)
    {
        $this->serialNumber = $serialNumber;

        return $this;
    }

    /**
     * Get serialNumber
     *
     * @return string 
     */
    public function getSerialNumber()
    {
        return $this->serialNumber;
    }

    /**
     * Set availableFrom
     *
     * @param \DateTime $availableFrom
     * @return Product
     */
    public function setAvailableFrom($availableFrom)
    {
        $this->availableFrom = $availableFrom;

        return $this;
    }

    /**
     * Get availableFrom
     *
     * @return \DateTime 
     */
    public function getAvailableFrom()
    {
        return $this->availableFrom;
    }
}
