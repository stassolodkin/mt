<?php

namespace Stas\SystemBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Document
 *
 * @ORM\Table(name="document")
 * @ORM\Entity
 */
class Document extends SystemModel {

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	protected $id;

	/**
	 * @ORM\Column(type="blob")
	 */
	protected $imageData;

	/** @ORM\Column(type="string") */
	protected $imageType;

	/** @ORM\Column(type="integer") */
	protected $imageLength;

	/** @ORM\Column(type="integer") */
	protected $imageWidth;

	/** @ORM\Column(type="integer") */
	protected $imageHeight;

	/** Relationship definitions */

	/**
	 * @var Identification $identification
	 *
	 * @ORM\ManyToOne(targetEntity="Identification", inversedBy="documents")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="identification_id", referencedColumnName="id")
	 * })
	 */
	private $identification;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set imageData
     *
     * @param string $imageData
     * @return Document
     */
    public function setImageData($imageData)
    {
        $this->imageData = $imageData;

        return $this;
    }

    /**
     * Get imageData
     *
     * @return string 
     */
    public function getImageData()
    {
        return $this->imageData;
    }

    /**
     * Set imageType
     *
     * @param string $imageType
     * @return Document
     */
    public function setImageType($imageType)
    {
        $this->imageType = $imageType;

        return $this;
    }

    /**
     * Get imageType
     *
     * @return string 
     */
    public function getImageType()
    {
        return $this->imageType;
    }

    /**
     * Set imageLength
     *
     * @param $imageLength
     * @return Document
     */
    public function setImageLength($imageLength)
    {
        $this->imageLength = intval($imageLength);

        return $this;
    }

    /**
     * Get imageLength
     *
     * @return integer
     */
    public function getImageLength()
    {
        return $this->imageLength;
    }

	/**
	 * Set identification
	 *
	 * @param $identification
	 * @return Document
	 */
	public function setIdentification($identification)
	{
		$this->identification = $identification;

		return $this;
	}

	/**
	 * Get imageLength
	 *
	 * @return Identification
	 */
	public function getIdentification()
	{
		return $this->identification;
	}

    /**
     * Set imageWidth
     *
     * @param $imageWidth
     * @return Document
     */
    public function setImageWidth($imageWidth)
    {
        $this->imageWidth = intval($imageWidth);

        return $this;
    }

    /**
     * Get imageWidth
     *
     * @return integer
     */
    public function getImageWidth()
    {
        return $this->imageWidth;
    }

    /**
     * Set imageHeight
     *
     * @param $imageHeight
     * @return Document
     */
    public function setImageHeight($imageHeight)
    {
        $this->imageHeight = intval($imageHeight);

        return $this;
    }

    /**
     * Get imageHeight
     *
     * @return integer
     */
    public function getImageHeight()
    {
        return $this->imageHeight;
    }
}
