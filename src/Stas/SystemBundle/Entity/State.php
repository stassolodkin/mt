<?php

namespace Stas\SystemBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection as ArrayCollection;

/**
 * State
 *
 * @ORM\Table(name="state")
 * @ORM\Entity
 */
class State extends SystemModel {

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	protected $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="string", length=100, nullable=false)
	 */
	private $name;

	/** Reverse relationships */

	/**
	 * @var ArrayCollection $addresses
	 *
	 * @ORM\OneToMany(targetEntity="Address", mappedBy="state")
	 */
	protected $addresses;

	/**
	 * Set name
	 *
	 * @param string $name
	 * @return State
	 */
	public function setName($name) {

		$this->name = $name;

		return $this;
	}

	/**
	 * Get name
	 *
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->addresses = new ArrayCollection();
    }

    /**
     * Add address
     *
     * @param Address $address
     * @return State
     */
    public function addAddress(Address $address)
    {
        $this->addresses->set($address->getId(), $address);

        return $this;
    }

    /**
     * Remove addresses
     *
     * @param Address $address
     */
    public function removeAddress(Address $address)
    {
        $this->addresses->remove($address->getId());
    }

    /**
     * Get addresses
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAddresses()
    {
        return $this->addresses;
    }
}
