<?php

namespace Stas\SystemBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection as ArrayCollection;

/**
 * Feature
 *
 * @ORM\Table(name="feature")
 * @ORM\Entity
 */
class Feature extends SystemModel {

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	protected $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="string", length=255, nullable=false)
	 */
	private $name;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="description", type="string", length=1024, nullable=true)
	 */
	private $description;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="key", type="string", length=255, nullable=true)
	 */
	private $key;


	/** Relationships */

	/**
	 * @ORM\ManyToMany(targetEntity="Account", mappedBy="accountFeatures")
	 **/
	private $accounts;

	/**
	 * @ORM\ManyToMany(targetEntity="Menu", mappedBy="features")
	 **/
	private $menu;

	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->accounts = new ArrayCollection();
		$this->menu = new ArrayCollection();
	}

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set description
	 *
	 * @param string $name
	 * @return Feature
	 */
	public function setDescription($name)
	{
		$this->name = $name;

		return $this;
	}

	/**
	 * Get description
	 *
	 * @return string
	 */
	public function getDescription()
	{
		return $this->description;
	}

	/**
	 * Set name
	 *
	 * @param string $name
	 * @return Feature
	 */
	public function setName($name)
	{
		$this->description = $name;

		return $this;
	}

	/**
	 * Get name
	 *
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * Set key
	 *
	 * @param string $key
	 * @return Feature
	 */
	public function setKey($key)
	{
		$this->key = $key;

		return $this;
	}

	/**
	 * Get key
	 *
	 * @return string
	 */
	public function getKey()
	{
		return $this->key;
	}

	/**
	 * Add account
	 *
	 * @param Account $account
	 * @return Account
	 */
	public function addAccount(Account $account) {
		$this->accounts->set($account->getId(), $account);

		return $this;
	}

	/**
	 * Remove account
	 *
	 * @param Account $account
	 */
	public function removeAccount(Account $account) {
		$this->accounts->remove($account->getId());
	}

	/**
	 * Get accounts
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getAccounts() {
		return $this->accounts;
	}

	/**
	 * Add menu
	 *
	 * @param Menu $menu
	 * @return Menu
	 */
	public function addMenu(Menu $menu) {
		$this->menu->set($menu->getId(), $menu);

		return $this;
	}

	/**
	 * Remove menu
	 *
	 * @param Menu $menu
	 */
	public function removeMenu(Menu $menu) {
		$this->menu->remove($menu->getId());
	}

	/**
	 * Get menu
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getMenu() {
		return $this->menu;
	}
}
