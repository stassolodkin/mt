<?php

namespace Stas\SystemBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection as ArrayCollection;

/**
 * Contact
 *
 * @ORM\Table(name="contact")
 * @ORM\Entity
 */
class Contact extends SystemModel {

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	protected $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="firstName", type="string", length=100, nullable=false)
	 */
	private $firstName;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="middleName", type="string", length=100, nullable=true)
	 */
	private $middleName;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="lastName", type="string", length=100, nullable=false)
	 */
	private $lastName;

	/** Relationships */

	/**
	 * @var Account $account
	 *
	 * @ORM\ManyToOne(targetEntity="Account", inversedBy="contacts")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="account_id", referencedColumnName="id")
	 * })
	 */
	private $account;

	/** Reverse relationships */

	/**
	 * @var ArrayCollection $identifications
	 *
	 * @ORM\OneToMany(targetEntity="Identification", mappedBy="contact")
	 */
	protected $identifications;

	/**
	 * @var ArrayCollection $phoneNumbers
	 *
	 * @ORM\OneToMany(targetEntity="PhoneNumber", mappedBy="contact")
	 */
	protected $phoneNumbers;

	/**
	 * @var ArrayCollection $emailAddresses
	 *
	 * @ORM\OneToMany(targetEntity="EmailAddress", mappedBy="contact")
	 */
	protected $emailAddresses;

	/**
	 * @var ArrayCollection $addresses
	 *
	 * @ORM\OneToMany(targetEntity="Address", mappedBy="contact")
	 */
	protected $addresses;

	/**
	 * @ORM\OneToOne(targetEntity="User", mappedBy="contact")
	 **/
	private $user;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->identifications = new ArrayCollection();
        $this->phoneNumbers = new ArrayCollection();
        $this->emailAddresses = new ArrayCollection();
        $this->addresses = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     * @return Contact
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string 
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set middleName
     *
     * @param string $middleName
     * @return Contact
     */
    public function setMiddleName($middleName)
    {
        $this->middleName = $middleName;

        return $this;
    }

    /**
     * Get middleName
     *
     * @return string 
     */
    public function getMiddleName()
    {
        return $this->middleName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     * @return Contact
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string 
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Add identifications
     *
     * @param Identification $identification
     * @return Contact
     */
    public function addIdentification(Identification $identification)
    {
        $this->identifications->set($identification->getId(), $identification);

        return $this;
    }

    /**
     * Remove identification
     *
     * @param Identification $identification
     */
    public function removeIdentification(Identification $identification)
    {
        $this->identifications->remove($identification->getId());
    }

    /**
     * Get identifications
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getIdentifications()
    {
        return $this->identifications;
    }

    /**
     * Add phoneNumbers
     *
     * @param PhoneNumber $phoneNumber
     * @return Contact
     */
    public function addPhoneNumber(PhoneNumber $phoneNumber)
    {
        $this->phoneNumbers->set($phoneNumber->getId(), $phoneNumber);

        return $this;
    }

    /**
     * Remove phoneNumber
     *
     * @param PhoneNumber $phoneNumber
     */
    public function removePhoneNumber(PhoneNumber $phoneNumber)
    {
        $this->phoneNumbers->remove($phoneNumber->getId());
    }

    /**
     * Get phoneNumbers
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPhoneNumbers() {
        return $this->phoneNumbers;
    }

    /**
     * Add emailAddresses
     *
     * @param EmailAddress $emailAddress
     * @return Contact
     */
    public function addEmailAddress(EmailAddress $emailAddress) {
        $this->emailAddresses->set($emailAddress->getId(), $emailAddress);

        return $this;
    }

    /**
     * Remove emailAddress
     *
     * @param EmailAddress $emailAddress
     */
    public function removeEmailAddress(EmailAddress $emailAddress) {
        $this->emailAddresses->remove($emailAddress->getId());
    }

    /**
     * Get emailAddresses
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEmailAddresses()
    {
        return $this->emailAddresses;
    }

    /**
     * Add addresses
     *
     * @param Address $address
     * @return Contact
     */
    public function addAddress(Address $address)
    {
        $this->addresses->set($address->getId(), $address);

        return $this;
    }

    /**
     * Remove address
     *
     * @param Address $address
     */
    public function removeAddress(Address $address)
    {
        $this->addresses->remove($address->getId());
    }

    /**
     * Get addresses
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAddresses()
    {
        return $this->addresses;
    }

    /**
     * Set user
     *
     * @param User $user
     * @return Contact
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Stas\SystemBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set account
     *
     * @param \Stas\SystemBundle\Entity\Account $account
     * @return Contact
     */
    public function setAccount(Account $account = null)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return \Stas\SystemBundle\Entity\Account 
     */
    public function getAccount()
    {
        return $this->account;
    }
}
