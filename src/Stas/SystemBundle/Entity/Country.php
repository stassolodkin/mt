<?php

namespace Stas\SystemBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection as ArrayCollection;
use Stas\SystemBundle\Entity\BO\ProductBrand;

/**
 * Country
 *
 * @ORM\Table(name="country")
 * @ORM\Entity
 */
class Country extends SystemModel {

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	protected $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="string", length=100, nullable=false)
	 */
	private $name;

	/** Reverse relationships */

	/**
	 * @var ArrayCollection $addresses
	 *
	 * @ORM\OneToMany(targetEntity="Address", mappedBy="country")
	 */
	protected $addresses;

	/**
	 * @var ArrayCollection $identifications
	 *
	 * @ORM\OneToMany(targetEntity="Identification", mappedBy="country")
	 */
	protected $identifications;

    /**
     * @var ArrayCollection $productBrands
     *
     * @ORM\OneToMany(targetEntity="Stas\SystemBundle\Entity\BO\ProductBrand", mappedBy="country")
     */
    protected $productBrands;

	/**
	 * Set name
	 *
	 * @param string $name
	 * @return Country
	 */
	public function setName($name) {

		$this->name = $name;

		return $this;
	}

	/**
	 * Get name
	 *
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->addresses = new ArrayCollection();
        $this->identifications = new ArrayCollection();
    }

    /**
     * Add address
     *
     * @param Address $address
     * @return Country
     */
    public function addAddress(Address $address)
    {
        $this->addresses->set($address->getId(), $address);

        return $this;
    }

    /**
     * Remove addresses
     *
     * @param Address $addresses
     */
    public function removeAddress(Address $addresses)
    {
        $this->addresses->removeElement($addresses);
    }

    /**
     * Get addresses
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAddresses()
    {
        return $this->addresses;
    }

    /**
     * Add identification
     *
     * @param Identification $identification
     * @return Country
     */
    public function addIdentification(Identification $identification)
    {
        $this->identifications->set($identification->getId(), $identification);

        return $this;
    }

    /**
     * Remove identification
     *
     * @param Identification $identification
     */
    public function removeIdentification(Identification $identification)
    {
        $this->identifications->remove($identification->getId());
    }

    /**
     * Get identifications
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getIdentifications()
    {
        return $this->identifications;
    }

    /**
     * Add productBrands
     *
     * @param \Stas\SystemBundle\Entity\BO\ProductBrand $productBrands
     * @return Country
     */
    public function addProductBrand(\Stas\SystemBundle\Entity\BO\ProductBrand $productBrands)
    {
        $this->productBrands[] = $productBrands;

        return $this;
    }

    /**
     * Remove productBrands
     *
     * @param \Stas\SystemBundle\Entity\BO\ProductBrand $productBrands
     */
    public function removeProductBrand(\Stas\SystemBundle\Entity\BO\ProductBrand $productBrands)
    {
        $this->productBrands->removeElement($productBrands);
    }

    /**
     * Get productBrands
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProductBrands()
    {
        return $this->productBrands;
    }
}
