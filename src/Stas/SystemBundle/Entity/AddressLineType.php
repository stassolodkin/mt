<?php

namespace Stas\SystemBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection as ArrayCollection;

/**
 * AddressLineType
 *
 * @ORM\Table(name="address_line_type")
 * @ORM\Entity
 */
class AddressLineType extends SystemModel {

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	protected $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="value", type="string", length=100, nullable=false)
	 */
	private $value;

	/** Reverse relationships */

	/**
	 * @var ArrayCollection $addressLines
	 * 
	 * @ORM\OneToMany(targetEntity="AddressLine", mappedBy="type")
	 */
	protected $addressLines;

	/**
	 * Set name
	 *
	 * @param string $value
	 * @return AddressLineType
	 */
	public function setValue($value) {

		$this->value = $value;

		return $this;
	}

	/**
	 * Get value
	 *
	 * @return string
	 */
	public function getValue() {
		return $this->value;
	}

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->addressLines = new ArrayCollection();
    }

    /**
     * Add addressLines
     *
     * @param AddressLine $addressLine
     * @return AddressLineType
     */
    public function addAddressLine(AddressLine $addressLine)
    {
        $this->addressLines->set($addressLine->getId(), $addressLine);

        return $this;
    }

    /**
     * Remove addressLine
     *
     * @param AddressLine $addressLine
     */
    public function removeAddressLine(AddressLine $addressLine)
    {
        $this->addressLines->remove($addressLine->getId());
    }

    /**
     * Get addressLines
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAddressLines()
    {
        return $this->addressLines;
    }
}
