<?php

namespace Stas\SystemBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection as ArrayCollection;

/**
 * AccountGroup
 *
 * @ORM\Table(name="account_group")
 * @ORM\Entity
 */
class AccountGroup extends SystemModel {

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	protected $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="description", type="string", length=1024, nullable=false)
	 */
	private $description;


	/** Relationships */

	/**
	 * Account that has created and owns this group
	 * @var Account $parentAccount
	 *
	 * @ORM\ManyToOne(targetEntity="Account", inversedBy="childAccountGroups")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="parent_account_id", referencedColumnName="id")
	 * })
	 */
	private $parentAccount;

	/**
	 * @ORM\ManyToMany(targetEntity="Account", mappedBy="accountGroups")
	 **/
	private $accounts;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->accounts = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return AccountGroup
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set parentAccount
     *
     * @param \Stas\SystemBundle\Entity\Account $parentAccount
     * @return AccountGroup
     */
    public function setParentAccount(Account $parentAccount = null)
    {
        $this->parentAccount = $parentAccount;

        return $this;
    }

    /**
     * Get parentAccount
     *
     * @return \Stas\SystemBundle\Entity\Account 
     */
    public function getParentAccount()
    {
        return $this->parentAccount;
    }

    /**
     * Add accounts
     *
     * @param Account $accounts
     * @return AccountGroup
     */
    public function addAccount(Account $accounts)
    {
        $this->accounts[] = $accounts;

        return $this;
    }

    /**
     * Remove account
     *
     * @param Account $account
     */
    public function removeAccount(Account $account)
    {
        $this->accounts->remove($account->getId());
    }

    /**
     * Get accounts
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAccounts()
    {
        return $this->accounts;
    }
}
