<?php

namespace Stas\SystemBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection as ArrayCollection;

/**
 * PhoneNumberType
 *
 * @ORM\Table(name="phone_number_type")
 * @ORM\Entity
 */
class PhoneNumberType extends SystemModel {

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	protected $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="value", type="string", length=100, nullable=false)
	 */
	private $value;

	/** Reverse relationships */

	/**
	 * @var ArrayCollection $phoneNumbers
	 *
	 * @ORM\OneToMany(targetEntity="PhoneNumber", mappedBy="type")
	 */
	protected $phoneNumbers;

	/**
	 * Set value
	 *
	 * @param string $value
	 * @return PhoneNumberType
	 */
	public function setValue($value) {

		$this->value = $value;

		return $this;
	}

	/**
	 * Get value
	 *
	 * @return string
	 */
	public function getValue() {
		return $this->value;
	}

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->phoneNumbers = new ArrayCollection();
    }

    /**
     * Add phoneNumber
     *
     * @param PhoneNumber $phoneNumber
     * @return PhoneNumberType
     */
    public function addPhoneNumber(PhoneNumber $phoneNumber)
    {
        $this->phoneNumbers->set($phoneNumber->getId(), $phoneNumber);

        return $this;
    }

    /**
     * Remove phoneNumber
     *
     * @param PhoneNumber $phoneNumber
     */
    public function removePhoneNumber(PhoneNumber $phoneNumber)
    {
        $this->phoneNumbers->remove($phoneNumber->getId());
    }

    /**
     * Get phoneNumbers
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPhoneNumbers()
    {
        return $this->phoneNumbers;
    }
}
