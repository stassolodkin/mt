<?php

namespace Stas\SystemBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection as ArrayCollection;

/**
 * Identification
 *
 * @ORM\Table(name="identification")
 * @ORM\Entity
 */
class Identification extends SystemModel {

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	protected $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="number", type="string", length=100, nullable=false)
	 */
	private $number;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="issuer", type="string", length=100, nullable=true)
	 */
	private $issuer;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="expiry_date", type="datetime", nullable=true)
	 */
	private $expiryDate;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="verified", type="boolean", nullable=true)
	 */
	private $verified;


	/** Relationship definitions */

	/**
	 * @var Country
	 *
	 * @ORM\ManyToOne(targetEntity="Country", inversedBy="identifications")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="country_id", referencedColumnName="id")
	 * })
	 */
	private $country;

	/**
	 * @var IdentificationType
	 *
	 * @ORM\ManyToOne(targetEntity="IdentificationType", inversedBy="identifications")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="type_id", referencedColumnName="id")
	 * })
	 */
	private $type;

	/**
	 * @var Contact
	 *
	 * @ORM\ManyToOne(targetEntity="Contact", inversedBy="identifications")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="contact_id", referencedColumnName="id")
	 * })
	 */
	private $contact;

	/** Reverse relationships */

	/**
	 * @var ArrayCollection $documents
	 *
	 * @ORM\OneToMany(targetEntity="Document", mappedBy="identification")
	 */
	protected $documents;



	/** Functions */

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Set number
	 *
	 * @param string $number
	 * @return Identification
	 */
	public function setNumber($number) {

		$this->number = $number;

		return $this;
	}

	/**
	 * Get number
	 *
	 * @return string
	 */
	public function getNumber() {
		return $this->number;
	}

	/**
	 * Set issuer
	 *
	 * @param string $issuer
	 * @return Identification
	 */
	public function setIssuer($issuer) {

		$this->issuer = $issuer;

		return $this;
	}

	/**
	 * Get issuer
	 *
	 * @return string
	 */
	public function getIssuer() {
		return $this->issuer;
	}



	/**
	 * Set country
	 *
	 * @param Country $country
	 * @return Identification
	 */
	public function setCountry($country) {

		$this->country = $country;

		return $this;
	}

	/**
	 * Get country
	 *
	 * @return Country
	 */
	public function getCountry() {
		return $this->country;
	}




	/**
	 * Get expiryDate
	 *
	 * @return \DateTime|null
	 */
	public function getExpiryDate() {

		return $this->expiryDate ? clone $this->expiryDate : null;
	}


	/**
	 * Set expiryDate
	 *
	 * @param \DateTime|null $expiryDate
	 * @return Identification
	 */
	public function setExpiryDate(\DateTime $expiryDate = null) {

		$this->expiryDate = $expiryDate ? clone $expiryDate : null;
		return $this;
	}

	/**
	 * Get contact
	 *
	 * @return Contact|null
	 */
	public function getContact() {

		return $this->contact;
	}


	/**
	 * Set contact
	 *
	 * @param Contact|null $contact
	 * @return Identification
	 */
	public function setContact(Contact $contact) {

		$this->contact = $contact;
		return $this;
	}

	/**
	 * Set type
	 *
	 * @param IdentificationType $type
	 * @return Identification
	 */
	public function setType($type) {

		$this->type = $type;

		return $this;
	}

	/**
	 * Get type
	 *
	 * @return IdentificationType
	 */
	public function getType() {
		return $this->type;
	}

	/**
	 * Get verified
	 *
	 * @return boolean
	 */
	public function getVerified() {
		return $this->verified;
	}

	/**
	 * Set verified
	 *
	 * @param boolean $verified
	 * @return Identification
	 */
	public function setVerified($verified = true) {

		$this->verified = $verified;

		return $this;
	}
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->documents = new ArrayCollection();
    }

    /**
     * Add document
     *
     * @param Document $document
     * @return Identification
     */
    public function addDocument(Document $document)
    {
        $this->documents->set($document->getId(), $document);

        return $this;
    }

    /**
     * Remove document
     *
     * @param Document $document
     */
    public function removeDocument(Document $document)
    {
        $this->documents->remove($document->getId());
    }

    /**
     * Get documents
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDocuments()
    {
        return $this->documents;
    }
}
