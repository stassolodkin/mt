<?php

namespace Stas\SystemBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection as ArrayCollection;

/**
 * Menu
 *
 * @ORM\Table(name="menu")
 * @ORM\Entity
 */
class Menu extends SystemModel {

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	protected $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="string", length=255, nullable=false)
	 */
	private $name;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="description", type="string", length=1024, nullable=true)
	 */
	private $description;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="order", type="integer", nullable=true)
	 */
	private $order;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="route", type="string", length=255, nullable=false)
	 */
	private $route;

    /**
     * @var string
     *
     * @ORM\Column(name="icon", type="string", length=255, nullable=false)
     */
    private $icon;


	/** Relationships */

	/**
	 * @var Menu $parentMenu
	 *
	 * @ORM\ManyToOne(targetEntity="Menu", inversedBy="children")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="parent_menu_id", referencedColumnName="id", nullable=true)
	 * })
	 */
	private $parentMenu;

	/**
	 * @var ArrayCollection $features
	 *
	 * @ORM\ManyToMany(targetEntity="Feature", inversedBy="menu")
	 * @ORM\JoinTable(name="menu_feature")
	 **/
	private $features;

	/** Reverse relationships */
	/**
	 * @var ArrayCollection $children
	 *
	 * @ORM\OneToMany(targetEntity="Menu", mappedBy="parentMenu")
	 */
	protected $children;

	/**
	 * Constructor
	 */
	public function __construct() {
		$this->accounts = new ArrayCollection();
		$this->features = new ArrayCollection();
		$this->children = new ArrayCollection();
	}

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Set description
	 *
	 * @param string $name
	 * @return Menu
	 */
	public function setDescription($name) {
		$this->name = $name;

		return $this;
	}

	/**
	 * Get description
	 *
	 * @return string
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * Set order
	 *
	 * @param integer $order
	 * @return Menu
	 */
	public function setOrder($order) {
		$this->order = $order;

		return $this;
	}

	/**
	 * Get order
	 *
	 * @return integer
	 */
	public function getOrder() {
		return $this->order;
	}

	/**
	 * Set parent Menu
	 *
	 * @param Menu $parent
	 * @return Menu
	 */
	public function setParent(Menu $parent) {
		$this->parentMenu = $parent;

		return $this;
	}

	/**
	 * Get parent Menu
	 *
	 * @return Menu
	 */
	public function getParent() {
		return $this->parentMenu;
	}

	/**
	 * Set name
	 *
	 * @param string $name
	 * @return Menu
	 */
	public function setName($name) {
		$this->description = $name;

		return $this;
	}

	/**
	 * Get name
	 *
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * Set route
	 *
	 * @param string $route
	 * @return Menu
	 */
	public function setRoute($route) {
		$this->route = $route;

		return $this;
	}

	/**
	 * Get route
	 *
	 * @return string
	 */
	public function getRoute() {
		return $this->route;
	}

    /**
     * Set icon
     *
     * @param string $route
     * @return Menu
     */
    public function setIcon($icon) {
        $this->icon = $icon;

        return $this;
    }

    /**
     * Get icon
     *
     * @return string
     */
    public function getIcon() {
        return $this->icon;
    }

	/**
	 * Add Feature
	 *
	 * @param Feature feature
	 * @return Menu
	 */
	public function addFeature(Feature $feature) {
		$this->features->set($feature->getId(), $feature);

		return $this;
	}

	/**
	 * Remove Feature
	 *
	 * @param Feature $feature
	 */
	public function removeFeature(Feature $feature) {
		$this->features->remove($feature->getId());
	}

	/**
	 * Get Features

	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getFeatures() {
		return $this->features;
	}

	/**
	 * Add child Menu
	 *
	 * @param Menu $childMenu
	 * @return Menu
	 */
	public function addChildMenu(Menu $childMenu) {
		$this->children->set($childMenu->getId(), $childMenu);

		return $this;
	}

	/**
	 * Remove child Menu
	 *
	 * @param Menu $childMenu
	 */
	public function removeChildMenu(Menu $childMenu) {
		$this->children->remove($childMenu->getId());
	}

	/**
	 * Get Children

	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getChildren() {
		return $this->children;
	}

	/** The following methods aee generated by Doctrine automatically and are not being used */

    /**
     * Set parentMenu
     *
     * @param \Stas\SystemBundle\Entity\Menu $parentMenu
     * @return Menu
     */
    public function setParentMenu(\Stas\SystemBundle\Entity\Menu $parentMenu = null)
    {
        $this->parentMenu = $parentMenu;

        return $this;
    }

    /**
     * Get parentMenu
     *
     * @return \Stas\SystemBundle\Entity\Menu 
     */
    public function getParentMenu()
    {
        return $this->parentMenu;
    }

    /**
     * Add children
     *
     * @param \Stas\SystemBundle\Entity\Menu $children
     * @return Menu
     */
    public function addChild(\Stas\SystemBundle\Entity\Menu $children)
    {
        $this->children[] = $children;

        return $this;
    }

    /**
     * Remove children
     *
     * @param \Stas\SystemBundle\Entity\Menu $children
     */
    public function removeChild(\Stas\SystemBundle\Entity\Menu $children)
    {
        $this->children->removeElement($children);
    }
}
