<?php

namespace Stas\SystemBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection as ArrayCollection;

/**
 * Account
 *
 * @ORM\Table(name="account")
 * @ORM\Entity
 */
class Account extends SystemModel {

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	protected $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="description", type="string", length=1024, nullable=false)
	 */
	private $description;


	/** Relationships */

	/**
	 * @var Account $parentAccount
	 *
	 * @ORM\ManyToOne(targetEntity="Account", inversedBy="childAccounts")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="parent_account_id", referencedColumnName="id", nullable=true)
	 * })
	 */
	private $parentAccount;
	
	/**
	 * @ORM\ManyToMany(targetEntity="AccountGroup", inversedBy="accounts")
	 * @ORM\JoinTable(name="account_account_group")
	 **/
	private $accountGroups;

	/**
	 * @ORM\ManyToMany(targetEntity="Feature", inversedBy="accounts")
	 * @ORM\JoinTable(name="account_account_feature")
	 **/
	private $accountFeatures;

	/** Reverse relationships */

	/**
	 * @var ArrayCollection $contacts
	 *
	 * @ORM\OneToMany(targetEntity="Contact", mappedBy="account")
	 */
	protected $contacts;

	/**
	 * @var ArrayCollection $childAccounts
	 *
	 * @ORM\OneToMany(targetEntity="Account", mappedBy="parentAccount")
	 */
	protected $childAccounts;

	/**
	 * Groups this account has created for its children
	 * @var ArrayCollection $childAccounts
	 *
	 * @ORM\OneToMany(targetEntity="AccountGroup", mappedBy="parentAccount")
	 */
	protected $childAccountGroups;
	/**
	 * Constructor
	 */
	public function __construct() {
		$this->accountGroups = new ArrayCollection();
		$this->accountFeatures = new ArrayCollection();
		$this->contacts = new ArrayCollection();
		$this->childAccounts = new ArrayCollection();
		$this->childAccountGroups = new ArrayCollection();
	}

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Set description
	 *
	 * @param string $description
	 * @return Account
	 */
	public function setDescription($description) {
		$this->description = $description;

		return $this;
	}

	/**
	 * Get description
	 *
	 * @return string
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * Set parentAccount
	 *
	 * @param Account $parentAccount
	 * @return Account
	 */
	public function setParentAccount(Account $parentAccount = null) {
		$this->parentAccount = $parentAccount;

		return $this;
	}

	/**
	 * Get parentAccount
	 *
	 * @return Account
	 */
	public function getParentAccount() {
		return $this->parentAccount;
	}

	/**
	 * Add accountGroup
	 *
	 * @param AccountGroup $accountGroup
	 * @return Account
	 */
	public function addAccountGroup(AccountGroup $accountGroup) {
		$this->accountGroups->set($accountGroup->getId(), $accountGroup);

		return $this;
	}

	/**
	 * Remove accountGroup
	 *
	 * @param AccountGroup $accountGroup
	 */
	public function removeAccountGroup(AccountGroup $accountGroup) {
		$this->accountGroups->remove($accountGroup->getId());
	}

	/**
	 * Get accountGroups
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getAccountGroups() {
		return $this->accountGroups;
	}

	/**
	 * Add Feature
	 *
	 * @param Feature $accountFeature
	 * @return Account
	 */
	public function addAccountFeature(Feature $accountFeature) {
		$this->accountFeatures->set($accountFeature->getId(), $accountFeature);

		return $this;
	}

	/**
	 * Remove Feature
	 *
	 * @param Feature $accountFeature
	 */
	public function removeAccountFeature(Feature $accountFeature) {
		$this->accountFeatures->remove($accountFeature->getId());
	}

	/**
	 * Get Features

	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getAccountFeatures() {
		return $this->accountFeatures;
	}

	/**
	 * Add contact
	 *
	 * @param Contact $contact
	 * @return Account
	 */
	public function addContact(Contact $contact) {
		$this->contacts->set($contact->getId(), $contact);

		return $this;
	}

	/**
	 * Remove contact
	 *
	 * @param Contact $contact
	 */
	public function removeContact(Contact $contact) {
		$this->contacts->remove($contact->getId());
	}

	/**
	 * Get contacts
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getContacts() {
		return $this->contacts;
	}

	/**
	 * Add childAccount
	 *
	 * @param Account $childAccount
	 * @return Account
	 */
	public function addChildAccount(Account $childAccount) {
		$this->childAccounts->set($childAccount->getId(), $childAccount);

		return $this;
	}

	/**
	 * Remove childAccount
	 *
	 * @param Account $childAccount
	 */
	public function removeChildAccount(Account $childAccount) {
		$this->childAccounts->remove($childAccount->getId());
	}

	/**
	 * Get childAccounts
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getChildAccounts() {
		return $this->childAccounts;
	}

	/**
	 * Add childAccountGroup
	 *
	 * @param AccountGroup $childAccountGroup
	 * @return Account
	 */
	public function addChildAccountGroup(AccountGroup $childAccountGroup) {
		$this->childAccountGroups->set($childAccountGroup->getId(), $childAccountGroup);

		return $this;
	}

	/**
	 * Remove childAccountGroup
	 *
	 * @param AccountGroup $childAccountGroup
	 */
	public function removeChildAccountGroup(AccountGroup $childAccountGroup) {
		$this->childAccountGroups->remove($childAccountGroup->getId());
	}

	/**
	 * Get childAccountGroups
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getChildAccountGroups() {
		return $this->childAccountGroups;
	}
}
