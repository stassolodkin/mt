<?php

namespace Stas\SystemBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection as ArrayCollection;

/**
 * EmailAddressType
 *
 * @ORM\Table(name="email_address_type")
 * @ORM\Entity
 */
class EmailAddressType extends SystemModel {

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	protected $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="value", type="string", length=100, nullable=false)
	 */
	private $value;

	/** Reverse relationships */

	/**
	 * @var ArrayCollection $emailAddresses
	 *
	 * @ORM\OneToMany(targetEntity="EmailAddress", mappedBy="type")
	 */
	protected $emailAddresses;

	/**
	 * Set value
	 *
	 * @param string $value
	 * @return EmailAddressType
	 */
	public function setValue($value) {

		$this->value = $value;

		return $this;
	}

	/**
	 * Get value
	 *
	 * @return string
	 */
	public function getValue() {
		return $this->value;
	}

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->emailAddresses = new ArrayCollection();
    }

    /**
     * Add emailAddress
     *
     * @param EmailAddress $emailAddress
     * @return EmailAddressType
     */
    public function addEmailAddress(EmailAddress $emailAddress)
    {
        $this->emailAddresses->set($emailAddress->getId(), $emailAddress);

        return $this;
    }

    /**
     * Remove emailAddress
     *
     * @param EmailAddress $emailAddress
     */
    public function removeEmailAddress(EmailAddress $emailAddress)
    {
        $this->emailAddresses->remove($emailAddress->getId());
    }

    /**
     * Get emailAddresses
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEmailAddresses()
    {
        return $this->emailAddresses;
    }
}
