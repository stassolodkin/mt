<?php
/**
 * Created by PhpStorm.
 * User: stassolodkin
 * Date: 14/08/15
 * Time: 1:02 AM
 */

namespace Stas\SystemBundle\Listener;


use Stas\SystemBundle\Entity\User as User;
use Stas\SystemBundle\Entity\Contact as Contact;
use Stas\SystemBundle\Entity\Account as Account;

use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\Security\Http\HttpUtils;
use Symfony\Component\Security\Http\Authentication\DefaultAuthenticationSuccessHandler;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\HttpFoundation\Request;
/**
 * Custom login listener.
 */
class LoginListener extends DefaultAuthenticationSuccessHandler {

	protected $router;
	protected $httpUtils;

    /**
     * LoginListener constructor.
     * @param HttpUtils $httpUtils
     * @param array $options
     * @param $router Router;
     */
	public function __construct( HttpUtils $httpUtils, array $options, $router) {

		$this->httpUtils = $httpUtils;
		$this->router = $router;
		parent::__construct( $httpUtils, $options );
	}

	public function onAuthenticationSuccess(Request $request, TokenInterface $token) {

		/** @var User $user */
		$user = $token->getUser();
		/** @var Contact $contact */
		$contact = $user->getContact();
		/** @var Account $account */
		$account = $contact->getAccount();

		$session = $request->getSession();
		$session->set('userId', $user->getId());
		$session->set('contactId', $contact->getId());
		$session->set('accountId', $account->getId());

		$referrer = $request->headers->get('referrer');
		if($referrer == NULL){
			$url = 'stas_system_homepage';
		}else{
			$url = $referrer;
		}

		$redirectUrl = $this->router->generate($url);

		return new RedirectResponse($redirectUrl);
	}
}