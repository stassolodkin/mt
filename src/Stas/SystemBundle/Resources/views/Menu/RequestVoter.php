<?php
/**
 * Created stassolodkin
 * Date: 27/12/15
 * Time: 9:56 AM
 *
 * This library is supposed to tell if a particular menu item is current by matching the current url with the items url
 * Not currently used, but is left here if a matcher ever needs to be used in this bundle's knp_menu.html.twig
 * At the moment its function is performed in js.
 *
 *
 */

namespace Stas\SystemBundle\Resources\views\Menu;

use Knp\Menu\ItemInterface;
use Knp\Menu\Matcher\Voter\VoterInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class RequestVoter implements VoterInterface {
    private $container;
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }
    public function matchItem(ItemInterface $item)
    {
        if ($item->getUri() === $this->container->get('request')->getRequestUri()) {
            // URL's completely match
            return true;
        } else if($item->getUri() !== '/' && (substr($this->container->get('request')->getRequestUri(), 0, strlen($item->getUri())) === $item->getUri())) {
            // URL isn't just "/" and the first part of the URL match
            return true;
        }
        return null;
    }

}