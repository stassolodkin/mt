<?php
/**
 * Created by PhpStorm.
 * User: stassolodkin
 * Date: 14/08/15
 * Time: 1:02 AM
 */

namespace Stas\SystemBundle\Service;


use Stas\SystemBundle\UIComponents as UI;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * UI Service
 */

class UIService {

	protected $container;

	public function __construct(ContainerInterface $container) {

		$this->container = $container;
	}

	/**
	 * @param $name
	 * @param $title
	 * @return UI\Panels
	 */
	public function getPanels($name, $title) {

		$panels = new UI\Panels($this->container->get('templating'), $name, $title);
		return $panels;
	}

	/**
	 * @param $name
	 * @param $title
	 * @return UI\Tabs
	 */
	public function getTabs($name, $title) {

		$tabs = new UI\Tabs($this->container->get('templating'), $name, $title);
		return $tabs;
	}

	/**
	 * @param $name
	 * @param $title
	 * @param null $data
	 * @return UI\Grid
	 */
	public function getGrid($name, $title, $data=null) {

		$grid = new UI\Grid($this->container->get('database_connection'), $this->container->get('templating'), $name, $title, $data);
		return $grid;
	}

    /**
     * @param $name
     * @param $title
     * @return UI\Form
     */
	public function getForm($name, $title) {

        /** @var FormFactory $formFactory */
        $formFactory = $this->container->get('form.factory');
		$form = new UI\Form($formFactory, $this->container->get('templating'), $name, $title);
        return $form;

	}
}