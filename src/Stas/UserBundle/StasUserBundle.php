<?php

namespace Stas\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class StasUserBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
